# Optimization Cases<a name="EN-US_TOPIC_0000001086338466"></a>

-   **[Case: Modifying the GUC Parameter rewrite\_rule](case-modifying-the-guc-parameter-rewrite_rule.md)**  

-   **[Case: Adjusting I/O Parameters to Reduce the Log Bloat Rate](case-adjusting-i-o-parameters-to-reduce-the-log-bloat-rate.md)**  


