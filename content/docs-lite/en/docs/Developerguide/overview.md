# Overview<a name="EN-US_TOPIC_0289899997"></a>

-   **[Database Logical Architecture](database-logical-architecture.md)**  

-   **[Query Request Handling Process](query-request-handling-process.md)**  

-   **[Managing Transactions](managing-transactions.md)**  

-   **[Concepts](concepts.md)**  


