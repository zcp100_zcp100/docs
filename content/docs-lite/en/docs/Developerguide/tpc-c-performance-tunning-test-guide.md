# TPC-C Performance Tunning Test Guide<a name="EN-US_TOPIC_0289900878"></a>

-   **[Overview](overview-17.md)**  

-   **[Setting up a Performance Test Environment](setting-up-a-performance-test-environment.md)**  

-   **[Testing TPC-C Performance](testing-tpc-c-performance.md)**  


