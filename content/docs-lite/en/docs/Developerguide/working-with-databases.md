# Working with Databases<a name="EN-US_TOPIC_0289900056"></a>

-   **[Before You Start](before-you-start.md)**  

-   **[Connecting to a Database](connecting-to-a-database.md)**  

-   **[Creating and Managing Databases](creating-and-managing-databases.md)**  

-   **[Planning a Storage Model](planning-a-storage-model.md)**  

-   **[Creating and Managing Tablespaces](creating-and-managing-tablespaces.md)**  

-   **[Creating and Managing Tables](creating-and-managing-tables.md)**  

-   **[Querying a System Catalog](querying-a-system-catalog.md)**  

-   **[Other Operations](other-operations.md)**  


