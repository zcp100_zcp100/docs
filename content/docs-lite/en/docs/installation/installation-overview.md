# Installation Overview<a name="EN-US_TOPIC_0289899723"></a>

openGauss Lite is installed separately on each node. Because the Lite edition does not have components such as the OM and CM, you need to manually perform OM functions such as installation, uninstallation, and upgrade on each node.

>![](public_sys-resources/icon-note.gif) **NOTE:** 
>During installation using the openGauss Lite, if it only needs to be installed on a single node, install it in standalone mode. After the uninstallation, check whether related environment variables need to be cleared. For details about the environment variables, see the following sections.

