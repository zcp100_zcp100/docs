# Preparations for Installation<a name="EN-US_TOPIC_0000001167470483"></a>

This section describes how to prepare and configure an environment for openGauss Lite installation. It can be installed on a single node or the primary and standby nodes. Read this section carefully before installation. If the configuration in this section has been completed, skip this section.

-   **[Obtaining the Installation Package](obtaining-the-installation-package.md)**  
You can obtain the installation package from the openGauss open-source community.
-   **[Preparing the Software and Hardware Installation Environment](preparing-the-software-and-hardware-installation-environment.md)**  


