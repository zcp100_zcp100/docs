# 源代码<a name="ZH-CN_TOPIC_0289899190"></a>

openGauss主要包含12个代码仓，引用开源软件的补丁代码仓、JDBC驱动代码仓、ODBC驱动代码仓、数据库服务器代码仓、数据库OM管理工具、数据库CM管理工具代码仓、数据库DCF代码仓、数据库DCC代码仓、数据库插件代码仓、MySQL到openGauss迁移工具代码仓、数据库prometheus-exporter代码仓和文档仓库，其中轻量版不涉及OM、CM代码仓：

-   开源软件代码仓：[https://gitee.com/opengauss/openGauss-third\_party](https://gitee.com/opengauss/openGauss-third_party)

-   JDBC驱动代码仓：[https://gitee.com/opengauss/openGauss-connector-jdbc](https://gitee.com/opengauss/openGauss-connector-jdbc)

-   ODBC驱动代码仓：[https://gitee.com/opengauss/openGauss-connector-odbc](https://gitee.com/opengauss/openGauss-connector-odbc)
-   数据库服务器代码仓：[https://gitee.com/opengauss/openGauss-server](https://gitee.com/opengauss/openGauss-server)
-   数据库OM工具代码仓：[https://gitee.com/opengauss/openGauss-OM](https://gitee.com/opengauss/openGauss-OM)
-   数据库CM\(Cluster Manager\)工具代码仓：[https://gitee.com/opengauss/CM](https://gitee.com/opengauss/CM)
-   数据库DCF\(Distributed Consensus Framework\)代码仓：[https://gitee.com/opengauss/DCF](https://gitee.com/opengauss/DCF)
-   数据库DCC\(Distributed Configuration Center\)代码仓：[https://gitee.com/opengauss/DCC](https://gitee.com/opengauss/DCC)
-   数据库插件代码仓：[https://gitee.com/opengauss/Plugin](https://gitee.com/opengauss/Plugin)
-   MySQL到openGauss迁移工具代码仓：[https://gitee.com/opengauss/openGauss-tools-chameleon](https://gitee.com/opengauss/openGauss-tools-chameleon)
-   数据库prometheus-exporter代码仓：[https://gitee.com/opengauss/openGauss-prometheus-exporter](https://gitee.com/opengauss/openGauss-prometheus-exporter)
-   数据库文档仓库：[https://gitee.com/opengauss/docs](https://gitee.com/opengauss/docs)

