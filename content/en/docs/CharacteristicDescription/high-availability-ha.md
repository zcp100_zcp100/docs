# High Availability \(HA\)<a name="EN-US_TOPIC_0000001135403987"></a>

-   **[Primary/Standby](primary-standby.md)**  

-   **[Logical Replication](logical-replication.md)**  

-   **[Logical Backup](logical-backup.md)**  

-   **[Physical Backup](physical-backup.md)**  

-   **[Automatic Job Retry upon Failure](automatic-job-retry-upon-failure.md)**  

-   **[Ultimate RTO](ultimate-rto.md)**  

-   **[Cascaded Standby Server](cascaded-standby-server.md)**  

-   **[Delayed Replay](delayed-replay.md)**  

-   **[Adding or Deleting a Standby Server](adding-or-deleting-a-standby-server.md)**  


