# Advanced Packages<a name="EN-US_TOPIC_0304085663"></a>

Advanced packages have two sets of interfaces. The first set is basic interfaces, and the second set is secondary encapsulation interfaces that are used improve usability. The second set is recommended.

-   **[Basic Interfaces](basic-interfaces.md)**  


