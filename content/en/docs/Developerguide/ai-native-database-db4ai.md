# AI-Native Database \(DB4AI\)<a name="EN-US_TOPIC_0000001102506722"></a>

DB4AI uses database capabilities to drive AI tasks and implement data storage and technology stack isomorphism. By integrating AI algorithms into the database, openGauss supports the native AI computing engine, model management, AI operators, and native AI execution plan, providing users with inclusive AI technologies. Different from the traditional AI modeling process, DB4AI one-stop modeling eliminates repeated data flowing among different platforms, simplifies the development process, and plans the optimal execution path through the database, so that developers can focus on the tuning of specific services and models. It outcompetes similar products in ease-of-use and performance.

-   **[DB4AI-Snapshots for Data Version Management](db4ai-snapshots-for-data-version-management.md)**  

-   **[DB4AI-Query for Model Training and Prediction](db4ai-query-for-model-training-and-prediction.md)**  

-   **[PL/Python Fenced Mode](pl-python-fenced-mode.md)**  


