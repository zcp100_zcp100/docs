# AI Sub-functions of the DBMind<a name="EN-US_TOPIC_0000001198483454"></a>

You can run the  **component**  subcommand of  **gs\_dbmind**  to enable the corresponding AI sub-functions. The following sections describe the AI functions in detail.

-   **[X-Tuner: Parameter Tuning and Diagnosis](x-tuner-parameter-tuning-and-diagnosis.md)**  

-   **[Index-advisor: Index Recommendation](index-advisor-index-recommendation.md)**  

-   **[AI4DB: Root Cause Analysis for Slow SQL Statements](ai4db-root-cause-analysis-for-slow-sql-statements.md)**  

-   **[AI4DB: Trend Prediction](ai4db-trend-prediction.md)**  

-   **[SQLdiag: Slow SQL Discovery](sqldiag-slow-sql-discovery.md)**  


