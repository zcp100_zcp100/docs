# Anomaly-detection: Database Metric Collection, Forecast, and Exception Detection<a name="EN-US_TOPIC_0296549224"></a>

-   **[Overview](overview-20.md)**  

-   **[Preparations](preparations-21.md)**  

-   **[Adding Monitoring Parameters](adding-monitoring-parameters.md)**  

-   **[Obtaining Help Information](obtaining-help-information-22.md)**  

-   **[Examples](examples-23.md)**  

-   **[Command Reference](command-reference-24.md)**  

-   **[AI\_SERVER](ai_server.md)**  

-   **[AI\_MANAGER](ai_manager.md)**  


