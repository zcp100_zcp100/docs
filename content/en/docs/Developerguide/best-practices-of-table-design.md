# Best Practices of Table Design<a name="EN-US_TOPIC_0000001119812464"></a>

-   **[Selecting a Storage Model](selecting-a-storage-model.md)**  

-   **[Using Partial Cluster Keys \(PCKs\)](using-partial-cluster-keys-(pcks).md)**  

-   **[Using Partitioned Tables](using-partitioned-tables.md)**  

-   **[Selecting a Data type](selecting-a-data-type.md)**  


