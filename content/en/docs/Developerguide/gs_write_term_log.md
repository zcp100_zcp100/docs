# GS\_WRITE\_TERM\_LOG<a name="EN-US_TOPIC_0000001212336584"></a>

**GS\_WRITE\_TERM\_LOG**  writes a log to record the current  **term**  value of the node. The standby node returns  **false**. After the data is successfully written to the primary node,  **true**  is returned.

