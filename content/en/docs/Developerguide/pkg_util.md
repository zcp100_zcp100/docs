# PKG\_UTIL<a name="EN-US_TOPIC_0304085686"></a>

[Table 1](#table35465232913)  lists all interfaces supported by the  **PKG\_UTIL**  package.

**Table  1**  PKG\_UTIL

<a name="table35465232913"></a>
<table><thead align="left"><tr id="row554714252919"><th class="cellrowborder" valign="top" width="49.96%" id="mcps1.2.3.1.1"><p id="p125470216296"><a name="p125470216296"></a><a name="p125470216296"></a>Interface</p>
</th>
<th class="cellrowborder" valign="top" width="50.03999999999999%" id="mcps1.2.3.1.2"><p id="p1354716216291"><a name="p1354716216291"></a><a name="p1354716216291"></a>Description</p>
</th>
</tr>
</thead>
<tbody><tr id="row2054718219291"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1954719211296"><a name="p1954719211296"></a><a name="p1954719211296"></a>PKG_UTIL.LOB_GET_LENGTH</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="en-us_topic_0059778749_a2589cef1af5e460aaa22a5cfa973ae2b"><a name="en-us_topic_0059778749_a2589cef1af5e460aaa22a5cfa973ae2b"></a><a name="en-us_topic_0059778749_a2589cef1af5e460aaa22a5cfa973ae2b"></a>Obtains the length of a LOB.</p>
</td>
</tr>
<tr id="row1154710220294"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1487415211311"><a name="p1487415211311"></a><a name="p1487415211311"></a>PKG_UTIL.LOB_READ</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="en-us_topic_0059778749_a53b5bdac784a44c7be9eb493bdbf4fbc"><a name="en-us_topic_0059778749_a53b5bdac784a44c7be9eb493bdbf4fbc"></a><a name="en-us_topic_0059778749_a53b5bdac784a44c7be9eb493bdbf4fbc"></a>Reads a part of a LOB.</p>
</td>
</tr>
<tr id="row12547527297"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p126045341312"><a name="p126045341312"></a><a name="p126045341312"></a>PKG_UTIL.LOB_WRITE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p5224175041213"><a name="p5224175041213"></a><a name="p5224175041213"></a>Writes the source object to the target object in the specified format.</p>
</td>
</tr>
<tr id="row1581764719397"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p3184174181311"><a name="p3184174181311"></a><a name="p3184174181311"></a>PKG_UTIL.LOB_APPEND</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p422115505128"><a name="p422115505128"></a><a name="p422115505128"></a>Appends a specified number of characters of the source LOB to the target LOB.</p>
</td>
</tr>
<tr id="row8603135417390"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p123139571310"><a name="p123139571310"></a><a name="p123139571310"></a>PKG_UTIL.LOB_COMPARE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p20217135016127"><a name="p20217135016127"></a><a name="p20217135016127"></a>Compares two LOBs based on the specified length.</p>
</td>
</tr>
<tr id="row3547142142917"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p14882759134"><a name="p14882759134"></a><a name="p14882759134"></a>PKG_UTIL.LOB_MATCH</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p621419507129"><a name="p621419507129"></a><a name="p621419507129"></a>Returns the position of the <em id="i294272719225"><a name="i294272719225"></a><a name="i294272719225"></a>N</em>th occurrence of a character string in a LOB.</p>
</td>
</tr>
<tr id="row10547192102916"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p193993641317"><a name="p193993641317"></a><a name="p193993641317"></a>PKG_UTIL.LOB_RESET</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p421285012126"><a name="p421285012126"></a><a name="p421285012126"></a>Resets the character in specified position of a LOB to a specified character.</p>
</td>
</tr>
<tr id="row754711272910"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p2010718813136"><a name="p2010718813136"></a><a name="p2010718813136"></a>PKG_UTIL.IO_PRINT</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1887613571510"><a name="p1887613571510"></a><a name="p1887613571510"></a>Displays character strings.</p>
</td>
</tr>
<tr id="row2278193519611"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p527819358611"><a name="p527819358611"></a><a name="p527819358611"></a>PKG_UTIL.RAW_GET_LENGTH</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p9441231383"><a name="p9441231383"></a><a name="p9441231383"></a>Obtains the length of <strong id="b164761458122212"><a name="b164761458122212"></a><a name="b164761458122212"></a>RAW</strong> data.</p>
</td>
</tr>
<tr id="row1577519455614"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p477511454617"><a name="p477511454617"></a><a name="p477511454617"></a>PKG_UTIL.RAW_CAST_FROM_VARCHAR2</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p377511458617"><a name="p377511458617"></a><a name="p377511458617"></a>Converts <strong id="b1675850192511"><a name="b1675850192511"></a><a name="b1675850192511"></a>VARCHAR2</strong> data to <strong id="b7691175811258"><a name="b7691175811258"></a><a name="b7691175811258"></a>RAW</strong> data.</p>
</td>
</tr>
<tr id="row164838411365"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p04841411263"><a name="p04841411263"></a><a name="p04841411263"></a>PKG_UTIL.RAW_CAST_FROM_BINARY_INTEGER</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p184843411962"><a name="p184843411962"></a><a name="p184843411962"></a>Converts binary integers to <strong id="b4141183917268"><a name="b4141183917268"></a><a name="b4141183917268"></a>RAW</strong> data.</p>
</td>
</tr>
<tr id="row587531468"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1087831664"><a name="p1087831664"></a><a name="p1087831664"></a>PKG_UTIL.RAW_CAST_TO_BINARY_INTEGER</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p18871131860"><a name="p18871131860"></a><a name="p18871131860"></a>Converts <strong id="b1481681519271"><a name="b1481681519271"></a><a name="b1481681519271"></a>RAW</strong> data to binary integers.</p>
</td>
</tr>
<tr id="row1823123816403"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p187606931314"><a name="p187606931314"></a><a name="p187606931314"></a>PKG_UTIL.SET_RANDOM_SEED</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p62051050201215"><a name="p62051050201215"></a><a name="p62051050201215"></a>Sets a random seed.</p>
</td>
</tr>
<tr id="row7522114154010"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1536010107139"><a name="p1536010107139"></a><a name="p1536010107139"></a>PKG_UTIL.RANDOM_GET_VALUE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p520219503128"><a name="p520219503128"></a><a name="p520219503128"></a>Returns a random value.</p>
</td>
</tr>
<tr id="row413212372417"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p21334322418"><a name="p21334322418"></a><a name="p21334322418"></a>PKG_UTIL.FILE_SET_DIRNAME</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p41331333245"><a name="p41331333245"></a><a name="p41331333245"></a>Sets the directory to be operated.</p>
</td>
</tr>
<tr id="row43111519172418"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p163121719192420"><a name="p163121719192420"></a><a name="p163121719192420"></a>PKG_UTIL.FILE_OPEN</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p4406468233"><a name="p4406468233"></a><a name="p4406468233"></a>Opens a file based on the specified file name and directory.</p>
</td>
</tr>
<tr id="row852351542415"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p552313152244"><a name="p552313152244"></a><a name="p552313152244"></a>PKG_UTIL.FILE_SET_MAX_LINE_SIZE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1152391514246"><a name="p1152391514246"></a><a name="p1152391514246"></a>Sets the maximum length of a line to be written to a file.</p>
</td>
</tr>
<tr id="row19447151012240"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1447171015242"><a name="p1447171015242"></a><a name="p1447171015242"></a>PKG_UTIL.FILE_IS_CLOSE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p644761012419"><a name="p644761012419"></a><a name="p644761012419"></a>Checks whether a file handle is closed.</p>
</td>
</tr>
<tr id="row34231534142415"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1042343442415"><a name="p1042343442415"></a><a name="p1042343442415"></a>PKG_UTIL.FILE_READ</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p124231634112412"><a name="p124231634112412"></a><a name="p124231634112412"></a>Reads data of a specified length from an open file handle.</p>
</td>
</tr>
<tr id="row2358257112012"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1845514419213"><a name="p1845514419213"></a><a name="p1845514419213"></a>PKG_UTIL.FILE_READLINE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1753787152118"><a name="p1753787152118"></a><a name="p1753787152118"></a>Reads a line of data from an open file handle.</p>
</td>
</tr>
<tr id="row10199114815241"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1619913487244"><a name="p1619913487244"></a><a name="p1619913487244"></a>PKG_UTIL.FILE_WRITE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p201991448102411"><a name="p201991448102411"></a><a name="p201991448102411"></a>Writes the data specified in the buffer to a file.</p>
</td>
</tr>
<tr id="row136569598247"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p12656115910246"><a name="p12656115910246"></a><a name="p12656115910246"></a>PKG_UTIL.FILE_WRITELINE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1476118251193"><a name="p1476118251193"></a><a name="p1476118251193"></a>Writes the buffer to a file and adds newline characters.</p>
</td>
</tr>
<tr id="row98831685916"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p16810151618595"><a name="p16810151618595"></a><a name="p16810151618595"></a>PKG_UTIL.FILE_NEWLINE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p18810516155912"><a name="p18810516155912"></a><a name="p18810516155912"></a>Adds a line.</p>
</td>
</tr>
<tr id="row1129313618917"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p112932619918"><a name="p112932619918"></a><a name="p112932619918"></a>PKG_UTIL.FILE_READ_RAW</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p152935613918"><a name="p152935613918"></a><a name="p152935613918"></a>Reads binary data of a specified length from an open file handle.</p>
</td>
</tr>
<tr id="row11051010195"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p51052010199"><a name="p51052010199"></a><a name="p51052010199"></a>PKG_UTIL.FILE_WRITE_RAW</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1010514101494"><a name="p1010514101494"></a><a name="p1010514101494"></a>Writes binary data to a file.</p>
</td>
</tr>
<tr id="row1339265318245"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p19392125312248"><a name="p19392125312248"></a><a name="p19392125312248"></a>PKG_UTIL.FILE_FLUSH</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1540818612237"><a name="p1540818612237"></a><a name="p1540818612237"></a>Writes data from a file handle to a physical file.</p>
</td>
</tr>
<tr id="row1559183710246"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p13591123762411"><a name="p13591123762411"></a><a name="p13591123762411"></a>PKG_UTIL.FILE_CLOSE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p174081672317"><a name="p174081672317"></a><a name="p174081672317"></a>Closes an open file handle.</p>
</td>
</tr>
<tr id="row1072014415251"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p07211546251"><a name="p07211546251"></a><a name="p07211546251"></a>PKG_UTIL.FILE_REMOVE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p107211412519"><a name="p107211412519"></a><a name="p107211412519"></a>Deletes a physical file. To do so, you must have the corresponding permission.</p>
</td>
</tr>
<tr id="row174807106252"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p448031015254"><a name="p448031015254"></a><a name="p448031015254"></a>PKG_UTIL.FILE_RENAME</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p84808107258"><a name="p84808107258"></a><a name="p84808107258"></a>Renames a file on the disk, similar to <strong id="b6942143916251"><a name="b6942143916251"></a><a name="b6942143916251"></a>mv</strong> in Unix.</p>
</td>
</tr>
<tr id="row13295644112417"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p8295544172417"><a name="p8295544172417"></a><a name="p8295544172417"></a>PKG_UTIL.FILE_SIZE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1295154418247"><a name="p1295154418247"></a><a name="p1295154418247"></a>Returns the size of a file.</p>
</td>
</tr>
<tr id="row466811361285"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p166692036172815"><a name="p166692036172815"></a><a name="p166692036172815"></a>PKG_UTIL.FILE_BLOCK_SIZE</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p166953619280"><a name="p166953619280"></a><a name="p166953619280"></a>Returns the number of blocks contained in a file.</p>
</td>
</tr>
<tr id="row5749848182815"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p974984812817"><a name="p974984812817"></a><a name="p974984812817"></a>PKG_UTIL.FILE_EXISTS</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p127491048172810"><a name="p127491048172810"></a><a name="p127491048172810"></a>Checks whether a file exists.</p>
</td>
</tr>
<tr id="row24629543281"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p5462754132815"><a name="p5462754132815"></a><a name="p5462754132815"></a>PKG_UTIL.FILE_GETPOS</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p946235410288"><a name="p946235410288"></a><a name="p946235410288"></a>Specifies the offset of a returned file, in bytes.</p>
</td>
</tr>
<tr id="row104681741112818"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p13468124112815"><a name="p13468124112815"></a><a name="p13468124112815"></a>PKG_UTIL.FILE_SEEK</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1946834111288"><a name="p1946834111288"></a><a name="p1946834111288"></a>Sets the offset for file position.</p>
</td>
</tr>
<tr id="row47052932812"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p127116290289"><a name="p127116290289"></a><a name="p127116290289"></a>PKG_UTIL.FILE_CLOSE_ALL</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p471129132811"><a name="p471129132811"></a><a name="p471129132811"></a>Closes all file handles opened in a session.</p>
</td>
</tr>
<tr id="row515112923310"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p015122914332"><a name="p015122914332"></a><a name="p015122914332"></a>PKG_UTIL.EXCEPTION_REPORT_ERROR</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p615162943311"><a name="p615162943311"></a><a name="p615162943311"></a>Throws an exception.</p>
</td>
</tr>
<tr id="row1447321618515"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1247301612515"><a name="p1247301612515"></a><a name="p1247301612515"></a>PKG_UTIL.RANDOM_SET_SEED</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p19473171613515"><a name="p19473171613515"></a><a name="p19473171613515"></a>Sets a random seed.</p>
</td>
</tr>
<tr id="row23051027144419"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p53061527104416"><a name="p53061527104416"></a><a name="p53061527104416"></a>pkg_util.app_read_client_info</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p7306182718443"><a name="p7306182718443"></a><a name="p7306182718443"></a>Reads the client information.</p>
</td>
</tr>
<tr id="row2862123119441"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p128621831184416"><a name="p128621831184416"></a><a name="p128621831184416"></a>pkg_util.app_set_client_info</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p2086213134415"><a name="p2086213134415"></a><a name="p2086213134415"></a>Sets the client information.</p>
</td>
</tr>
<tr id="row17951173414419"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p20952143413449"><a name="p20952143413449"></a><a name="p20952143413449"></a>pkg_util.lob_converttoblob</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p2952234144418"><a name="p2952234144418"></a><a name="p2952234144418"></a>Converts the CLOB type to the BLOB type.</p>
</td>
</tr>
<tr id="row294319379443"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p139431537124417"><a name="p139431537124417"></a><a name="p139431537124417"></a>pkg_util.lob_converttoclob</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p10943183724413"><a name="p10943183724413"></a><a name="p10943183724413"></a>Converts the BLOB type to the CLOB type.</p>
</td>
</tr>
<tr id="row1943234111449"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1043264111448"><a name="p1043264111448"></a><a name="p1043264111448"></a>pkg_util.lob_rawtotext</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p19432194134413"><a name="p19432194134413"></a><a name="p19432194134413"></a>Converts the raw type to the text type.</p>
</td>
</tr>
<tr id="row134834413442"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1734814412444"><a name="p1734814412444"></a><a name="p1734814412444"></a>pkg_util.lob_reset</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p63482044174410"><a name="p63482044174410"></a><a name="p63482044174410"></a>Clears data of the LOB type.</p>
</td>
</tr>
<tr id="row522963610467"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p162297360461"><a name="p162297360461"></a><a name="p162297360461"></a>pkg_util.lob_texttoraw</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p52291236184618"><a name="p52291236184618"></a><a name="p52291236184618"></a>Converts the text type to the raw type.</p>
</td>
</tr>
<tr id="row16571745114614"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p35711345104610"><a name="p35711345104610"></a><a name="p35711345104610"></a>pkg_util.lob_write</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p1757113456461"><a name="p1757113456461"></a><a name="p1757113456461"></a>Writes data to the LOB type.</p>
</td>
</tr>
<tr id="row194341242194612"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p19434642114612"><a name="p19434642114612"></a><a name="p19434642114612"></a>pkg_util.match_edit_distance_similarity</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p643417422461"><a name="p643417422461"></a><a name="p643417422461"></a>Calculates the difference between two character strings.</p>
</td>
</tr>
<tr id="row056515299478"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p13565122912479"><a name="p13565122912479"></a><a name="p13565122912479"></a>pkg_util.raw_cast_to_varchar2</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p856517297472"><a name="p856517297472"></a><a name="p856517297472"></a>Converts the raw type to the varchar2 type.</p>
</td>
</tr>
<tr id="row663323216476"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p2633032174713"><a name="p2633032174713"></a><a name="p2633032174713"></a>pkg_util.session_clear_context</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p2633193294717"><a name="p2633193294717"></a><a name="p2633193294717"></a>Clears the attribute values in <strong id="b8511141141712"><a name="b8511141141712"></a><a name="b8511141141712"></a>session_context</strong>.</p>
</td>
</tr>
<tr id="row19925152584710"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1592582534718"><a name="p1592582534718"></a><a name="p1592582534718"></a>pkg_util.session_search_context</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p4925225174717"><a name="p4925225174717"></a><a name="p4925225174717"></a>Searches for an attribute value.</p>
</td>
</tr>
<tr id="row27701459164710"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p14770185924715"><a name="p14770185924715"></a><a name="p14770185924715"></a>pkg_util.session_set_context</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p777055924714"><a name="p777055924714"></a><a name="p777055924714"></a>Sets an attribute value.</p>
</td>
</tr>
<tr id="row1992218224818"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1292214220480"><a name="p1292214220480"></a><a name="p1292214220480"></a>pkg_util.utility_format_call_stack</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p69227211485"><a name="p69227211485"></a><a name="p69227211485"></a>Displays the call stack of a stored procedure.</p>
</td>
</tr>
<tr id="row1632966144814"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p17329136114811"><a name="p17329136114811"></a><a name="p17329136114811"></a>pkg_util.utility_format_error_backtrace</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p43291664818"><a name="p43291664818"></a><a name="p43291664818"></a>Displays the error stack of a stored procedure.</p>
</td>
</tr>
<tr id="row186851811104919"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p106852011154919"><a name="p106852011154919"></a><a name="p106852011154919"></a>pkg_util.utility_format_error_stack</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p5685911134912"><a name="p5685911134912"></a><a name="p5685911134912"></a>Displays the error information about a stored procedure.</p>
</td>
</tr>
<tr id="row165711167497"><td class="cellrowborder" valign="top" width="49.96%" headers="mcps1.2.3.1.1 "><p id="p1565717167498"><a name="p1565717167498"></a><a name="p1565717167498"></a>pkg_util.utility_get_time</p>
</td>
<td class="cellrowborder" valign="top" width="50.03999999999999%" headers="mcps1.2.3.1.2 "><p id="p20657716134916"><a name="p20657716134916"></a><a name="p20657716134916"></a>Displays the Unix timestamp of the system.</p>
</td>
</tr>
</tbody>
</table>

-   PKG\_UTIL.LOB\_GET\_LENGTH

    This function obtains the length of the input data.

    The function prototype of  **PKG\_UTIL.LOB\_GET\_LENGTH**  is as follows:

    ```
    PKG_UTIL.LOB_GET_LENGTH(
    lob       IN   anyelement
    )
    RETURN INTEGER;
    ```

    **Table  2**  PKG\_UTIL.LOB\_GET\_LENGTH interface parameters

    <a name="table135221610538"></a>
    <table><thead align="left"><tr id="row17522111014315"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p6522151011319"><a name="p6522151011319"></a><a name="p6522151011319"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p12522131014316"><a name="p12522131014316"></a><a name="p12522131014316"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p1852271011312"><a name="p1852271011312"></a><a name="p1852271011312"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p552217101634"><a name="p552217101634"></a><a name="p552217101634"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p652211010315"><a name="p652211010315"></a><a name="p652211010315"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1852251013316"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p205229106319"><a name="p205229106319"></a><a name="p205229106319"></a>lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p552220104319"><a name="p552220104319"></a><a name="p552220104319"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p105223109318"><a name="p105223109318"></a><a name="p105223109318"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p65231109312"><a name="p65231109312"></a><a name="p65231109312"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p752381011312"><a name="p752381011312"></a><a name="p752381011312"></a>Indicates the object whose length is to be obtained.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.LOB\_READ

    This function reads an object and returns the specified part.

    The function prototype of  **PKG\_UTIL.LOB\_READ**  is as follows:

    ```
    PKG_UTIL.LOB_READ(
    lob       IN   anyelement,
    len       IN   int,
    start     IN   int,
    mode      IN   int
    )
    RETURN ANYELEMENT
    ```

    **Table  3**  PKG\_UTIL.LOB\_READ interface parameters

    <a name="table113589108264"></a>
    <table><thead align="left"><tr id="row2358710122616"><th class="cellrowborder" valign="top" width="14.05%" id="mcps1.2.6.1.1"><p id="p1135831016263"><a name="p1135831016263"></a><a name="p1135831016263"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="9.86%" id="mcps1.2.6.1.2"><p id="p1635812106266"><a name="p1635812106266"></a><a name="p1635812106266"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="12.41%" id="mcps1.2.6.1.3"><p id="p8358101013264"><a name="p8358101013264"></a><a name="p8358101013264"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p20358510162619"><a name="p20358510162619"></a><a name="p20358510162619"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.96999999999999%" id="mcps1.2.6.1.5"><p id="p13358191072618"><a name="p13358191072618"></a><a name="p13358191072618"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row535817108263"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p18359171016263"><a name="p18359171016263"></a><a name="p18359171016263"></a>lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="9.86%" headers="mcps1.2.6.1.2 "><p id="p173597102262"><a name="p173597102262"></a><a name="p173597102262"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="12.41%" headers="mcps1.2.6.1.3 "><p id="p163591610172612"><a name="p163591610172612"></a><a name="p163591610172612"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p93591710122618"><a name="p93591710122618"></a><a name="p93591710122618"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p2035951013267"><a name="p2035951013267"></a><a name="p2035951013267"></a>Specifies CLOB or BLOB data.</p>
    </td>
    </tr>
    <tr id="row76017513285"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p20617592815"><a name="p20617592815"></a><a name="p20617592815"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="9.86%" headers="mcps1.2.6.1.2 "><p id="p96195142815"><a name="p96195142815"></a><a name="p96195142815"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="12.41%" headers="mcps1.2.6.1.3 "><p id="p261752286"><a name="p261752286"></a><a name="p261752286"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p10614513284"><a name="p10614513284"></a><a name="p10614513284"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p4617562814"><a name="p4617562814"></a><a name="p4617562814"></a>Specifies the length of the returned result.</p>
    </td>
    </tr>
    <tr id="row115711096281"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p9571698286"><a name="p9571698286"></a><a name="p9571698286"></a>start</p>
    </td>
    <td class="cellrowborder" valign="top" width="9.86%" headers="mcps1.2.6.1.2 "><p id="p757112922819"><a name="p757112922819"></a><a name="p757112922819"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="12.41%" headers="mcps1.2.6.1.3 "><p id="p1557117916289"><a name="p1557117916289"></a><a name="p1557117916289"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1757129112817"><a name="p1757129112817"></a><a name="p1757129112817"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p12571493281"><a name="p12571493281"></a><a name="p12571493281"></a>Specifies the offset to the first character.</p>
    </td>
    </tr>
    <tr id="row1314951411287"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p141500141281"><a name="p141500141281"></a><a name="p141500141281"></a>mode</p>
    </td>
    <td class="cellrowborder" valign="top" width="9.86%" headers="mcps1.2.6.1.2 "><p id="p81501314102811"><a name="p81501314102811"></a><a name="p81501314102811"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="12.41%" headers="mcps1.2.6.1.3 "><p id="p121501914102818"><a name="p121501914102818"></a><a name="p121501914102818"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1615091416289"><a name="p1615091416289"></a><a name="p1615091416289"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p81501314122810"><a name="p81501314122810"></a><a name="p81501314122810"></a>Specifies the type of the read operation. <strong id="b1367319327428"><a name="b1367319327428"></a><a name="b1367319327428"></a>0</strong> indicates <strong id="b1969616356483"><a name="b1969616356483"></a><a name="b1969616356483"></a>READ</strong>, <strong id="b826644074211"><a name="b826644074211"></a><a name="b826644074211"></a>1</strong> indicates <strong id="b10896941144817"><a name="b10896941144817"></a><a name="b10896941144817"></a>TRIM</strong>, and <strong id="b54028509425"><a name="b54028509425"></a><a name="b54028509425"></a>2</strong> indicates <strong id="b272831544915"><a name="b272831544915"></a><a name="b272831544915"></a>SUBSTR</strong>.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.LOB\_WRITE

    This function writes the source object to the target object based on the specified parameters and returns the target object.

    The function prototype of  **PKG\_UTIL.LOB\_WRITE**  is as follows:

    ```
    PKG_UTIL.LOB_WRITE(
    dest_lob    INOUT   anyelement,
    src_lob     IN      varchar2
    len         IN      int,
    start       IN      int
    )
    RETURN ANYELEMENT;
    ```

    **Table  4**  PKG\_UTIL.LOB\_WRITE interface parameters

    <a name="table188631147184216"></a>
    <table><thead align="left"><tr id="row58631479422"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p1086318476420"><a name="p1086318476420"></a><a name="p1086318476420"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.450000000000001%" id="mcps1.2.6.1.2"><p id="p18863447194214"><a name="p18863447194214"></a><a name="p18863447194214"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="11.820000000000002%" id="mcps1.2.6.1.3"><p id="p2086374794214"><a name="p2086374794214"></a><a name="p2086374794214"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p486354774219"><a name="p486354774219"></a><a name="p486354774219"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p586364764212"><a name="p586364764212"></a><a name="p586364764212"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1886311478426"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p141041495499"><a name="p141041495499"></a><a name="p141041495499"></a>dest_lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.450000000000001%" headers="mcps1.2.6.1.2 "><p id="p38451191514"><a name="p38451191514"></a><a name="p38451191514"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.820000000000002%" headers="mcps1.2.6.1.3 "><p id="p11864747164216"><a name="p11864747164216"></a><a name="p11864747164216"></a>INOUT</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1986464710423"><a name="p1986464710423"></a><a name="p1986464710423"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p1186434713421"><a name="p1186434713421"></a><a name="p1186434713421"></a>Specifies the target object that data will be written to.</p>
    </td>
    </tr>
    <tr id="row13943934194915"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p4944193417493"><a name="p4944193417493"></a><a name="p4944193417493"></a>src_lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.450000000000001%" headers="mcps1.2.6.1.2 "><p id="p1994433411492"><a name="p1994433411492"></a><a name="p1994433411492"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.820000000000002%" headers="mcps1.2.6.1.3 "><p id="p1094483412499"><a name="p1094483412499"></a><a name="p1094483412499"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p17944193414917"><a name="p17944193414917"></a><a name="p17944193414917"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p1016315111505"><a name="p1016315111505"></a><a name="p1016315111505"></a>Specifies the source object to be written.</p>
    </td>
    </tr>
    <tr id="row3864104704212"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p1786418474426"><a name="p1786418474426"></a><a name="p1786418474426"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.450000000000001%" headers="mcps1.2.6.1.2 "><p id="p12864104713424"><a name="p12864104713424"></a><a name="p12864104713424"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.820000000000002%" headers="mcps1.2.6.1.3 "><p id="p38641347134217"><a name="p38641347134217"></a><a name="p38641347134217"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p2086524720424"><a name="p2086524720424"></a><a name="p2086524720424"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p17842151917509"><a name="p17842151917509"></a><a name="p17842151917509"></a>Specifies the write length of the source object.</p>
    </td>
    </tr>
    <tr id="row595113013452"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p29543018457"><a name="p29543018457"></a><a name="p29543018457"></a>start</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.450000000000001%" headers="mcps1.2.6.1.2 "><p id="p1295163011456"><a name="p1295163011456"></a><a name="p1295163011456"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.820000000000002%" headers="mcps1.2.6.1.3 "><p id="p1495153094519"><a name="p1495153094519"></a><a name="p1495153094519"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p10950306459"><a name="p10950306459"></a><a name="p10950306459"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p495113014515"><a name="p495113014515"></a><a name="p495113014515"></a>Specifies the write start position of the target object.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.LOB\_APPEND

    This function appends the source object to the target BLOB/CLOB and returns the target BLOB/CLOB.

    The function prototype of  **PKG\_UTIL.LOB\_APPEND**  is as follows:

    ```
    PKG_UTIL.LOB_APPEND(
    dest_lob    INOUT   blob,
    src_lob     IN      blob,
    len         IN      int default NULL
    )
    RETURN BLOB;
    
    PKG_UTIL.LOB_APPEND(
    dest_lob    INOUT   clob,
    src_lob     IN      clob,
    len         IN      int default NULL
    )
    RETURN CLOB;
    ```

    **Table  5**  PKG\_UTIL.LOB\_APPEND interface parameters

    <a name="table1596161974916"></a>
    <table><thead align="left"><tr id="row18596171944914"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p13596719174916"><a name="p13596719174916"></a><a name="p13596719174916"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p1659621911497"><a name="p1659621911497"></a><a name="p1659621911497"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p12597119144913"><a name="p12597119144913"></a><a name="p12597119144913"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p1597141919495"><a name="p1597141919495"></a><a name="p1597141919495"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p159712190497"><a name="p159712190497"></a><a name="p159712190497"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row259751918494"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p1766011171082"><a name="p1766011171082"></a><a name="p1766011171082"></a>dest_lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p159711913494"><a name="p159711913494"></a><a name="p159711913494"></a>blob/clob</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p659720192496"><a name="p659720192496"></a><a name="p659720192496"></a>INOUT</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p65971519174918"><a name="p65971519174918"></a><a name="p65971519174918"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p55971819144910"><a name="p55971819144910"></a><a name="p55971819144910"></a>Specifies the target BLOB/CLOB that data will be written to.</p>
    </td>
    </tr>
    <tr id="row1759721904910"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p459771917492"><a name="p459771917492"></a><a name="p459771917492"></a>src_lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p15972019114920"><a name="p15972019114920"></a><a name="p15972019114920"></a>blob/clob</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p10597181920496"><a name="p10597181920496"></a><a name="p10597181920496"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p13597319164912"><a name="p13597319164912"></a><a name="p13597319164912"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p259717191497"><a name="p259717191497"></a><a name="p259717191497"></a>Specifies the source BLOB/CLOB to be written.</p>
    </td>
    </tr>
    <tr id="row1159781913496"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p955419261814"><a name="p955419261814"></a><a name="p955419261814"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p1759791914495"><a name="p1759791914495"></a><a name="p1759791914495"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p115975192499"><a name="p115975192499"></a><a name="p115975192499"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1159820196497"><a name="p1159820196497"></a><a name="p1159820196497"></a>Yes</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p18598141934919"><a name="p18598141934919"></a><a name="p18598141934919"></a>Specifies the length of the source object to be written. If the value is <strong id="b1134644510442"><a name="b1134644510442"></a><a name="b1134644510442"></a>NULL</strong>, the entire source object is written by default.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.LOB\_COMPARE

    This function checks whether objects are the same based on the specified start position and size. If  **lob1**  is larger,  **1**  is returned. If  **lob2**  is larger,  **–1**  is returned. If  **lob1**  is equal to  **lob2**,  **0**  is returned.

    The function prototype of  **PKG\_UTIL.LOB\_COMPARE**  is as follows:

    ```
    PKG_UTIL.LOB_COMPARE(
    lob1        IN   anyelement,
    lob2        IN   anyelement,
    len         IN   int,
    start1      IN   int,
    start2      IN   int
    )
    RETURN INTEGER;
    ```

    **Table  6**  PKG\_UTIL.LOB\_COMPARE interface parameters

    <a name="table1461014365543"></a>
    <table><thead align="left"><tr id="row26101436165417"><th class="cellrowborder" valign="top" width="14.05%" id="mcps1.2.6.1.1"><p id="p36101736115410"><a name="p36101736115410"></a><a name="p36101736115410"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="11.52%" id="mcps1.2.6.1.2"><p id="p661083665413"><a name="p661083665413"></a><a name="p661083665413"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.75%" id="mcps1.2.6.1.3"><p id="p1461013635412"><a name="p1461013635412"></a><a name="p1461013635412"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p56101836175413"><a name="p56101836175413"></a><a name="p56101836175413"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.96999999999999%" id="mcps1.2.6.1.5"><p id="p19610193635411"><a name="p19610193635411"></a><a name="p19610193635411"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1361083685412"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p13610133635412"><a name="p13610133635412"></a><a name="p13610133635412"></a>lob1</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p17610153695417"><a name="p17610153695417"></a><a name="p17610153695417"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p161011361542"><a name="p161011361542"></a><a name="p161011361542"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p11611636105417"><a name="p11611636105417"></a><a name="p11611636105417"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p10611636175414"><a name="p10611636175414"></a><a name="p10611636175414"></a>Indicates the character string for comparison.</p>
    </td>
    </tr>
    <tr id="row1611163665415"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p5611536115413"><a name="p5611536115413"></a><a name="p5611536115413"></a>lob2</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p3611143695414"><a name="p3611143695414"></a><a name="p3611143695414"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p166111365547"><a name="p166111365547"></a><a name="p166111365547"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p9611133635415"><a name="p9611133635415"></a><a name="p9611133635415"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p1461118364545"><a name="p1461118364545"></a><a name="p1461118364545"></a>Indicates the character string for comparison.</p>
    </td>
    </tr>
    <tr id="row6611153665412"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p161193655412"><a name="p161193655412"></a><a name="p161193655412"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p16611536155419"><a name="p16611536155419"></a><a name="p16611536155419"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p186116363541"><a name="p186116363541"></a><a name="p186116363541"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1761115365541"><a name="p1761115365541"></a><a name="p1761115365541"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p18611183695418"><a name="p18611183695418"></a><a name="p18611183695418"></a>Indicates the length to be compared.</p>
    </td>
    </tr>
    <tr id="row561123619543"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p161133665411"><a name="p161133665411"></a><a name="p161133665411"></a>start1</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p2611736205418"><a name="p2611736205418"></a><a name="p2611736205418"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p4611153610547"><a name="p4611153610547"></a><a name="p4611153610547"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p176111736175412"><a name="p176111736175412"></a><a name="p176111736175412"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p46126364544"><a name="p46126364544"></a><a name="p46126364544"></a>Specifies the start offset of <strong id="b2914947125014"><a name="b2914947125014"></a><a name="b2914947125014"></a>lob1</strong>.</p>
    </td>
    </tr>
    <tr id="row20521172017712"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p852217201374"><a name="p852217201374"></a><a name="p852217201374"></a>start2</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p35221620577"><a name="p35221620577"></a><a name="p35221620577"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p155227201178"><a name="p155227201178"></a><a name="p155227201178"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p65227201073"><a name="p65227201073"></a><a name="p65227201073"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p145235203717"><a name="p145235203717"></a><a name="p145235203717"></a>Specifies the start offset of <strong id="b11499656135012"><a name="b11499656135012"></a><a name="b11499656135012"></a>lob2</strong>.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.LOB\_MATCH

    This function returns the position where a pattern is displayed in a LOB for the  _match\_n_th time.

    The function prototype of  **PKG\_UTIL.LOB\_MATCH**  is as follows:

    ```
    PKG_UTIL.LOB_MATCH(
    lob          IN   anyelement,
    pattern      IN   anyelement,
    start        IN   int,
    match_nth    IN   int
    )
    RETURN INTEGER;
    ```

    **Table  7**  PKG\_UTIL.LOB\_MATCH interface parameters

    <a name="table968599101615"></a>
    <table><thead align="left"><tr id="row1568599121616"><th class="cellrowborder" valign="top" width="14.05%" id="mcps1.2.6.1.1"><p id="p16852911610"><a name="p16852911610"></a><a name="p16852911610"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="11.52%" id="mcps1.2.6.1.2"><p id="p5685129121616"><a name="p5685129121616"></a><a name="p5685129121616"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.75%" id="mcps1.2.6.1.3"><p id="p668529101611"><a name="p668529101611"></a><a name="p668529101611"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p668612931611"><a name="p668612931611"></a><a name="p668612931611"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.96999999999999%" id="mcps1.2.6.1.5"><p id="p186869991614"><a name="p186869991614"></a><a name="p186869991614"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row56866981612"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p76861193163"><a name="p76861193163"></a><a name="p76861193163"></a>lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p1868614971611"><a name="p1868614971611"></a><a name="p1868614971611"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p1668616921616"><a name="p1668616921616"></a><a name="p1668616921616"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1368619961610"><a name="p1368619961610"></a><a name="p1368619961610"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p3686189201613"><a name="p3686189201613"></a><a name="p3686189201613"></a>Indicates the character string for comparison.</p>
    </td>
    </tr>
    <tr id="row96868912169"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p268615971619"><a name="p268615971619"></a><a name="p268615971619"></a>pattern</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p19686995168"><a name="p19686995168"></a><a name="p19686995168"></a>clob/blob</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p11686179141616"><a name="p11686179141616"></a><a name="p11686179141616"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p15686189151614"><a name="p15686189151614"></a><a name="p15686189151614"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p668718911161"><a name="p668718911161"></a><a name="p668718911161"></a>Specifies the pattern to be matched.</p>
    </td>
    </tr>
    <tr id="row66877991615"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p1668720919167"><a name="p1668720919167"></a><a name="p1668720919167"></a>start</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p126874914160"><a name="p126874914160"></a><a name="p126874914160"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p1968749101615"><a name="p1968749101615"></a><a name="p1968749101615"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p2068714914160"><a name="p2068714914160"></a><a name="p2068714914160"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p1842151520201"><a name="p1842151520201"></a><a name="p1842151520201"></a>Specifies the start position for LOB comparison.</p>
    </td>
    </tr>
    <tr id="row768711919161"><td class="cellrowborder" valign="top" width="14.05%" headers="mcps1.2.6.1.1 "><p id="p14687191162"><a name="p14687191162"></a><a name="p14687191162"></a>match_nth</p>
    </td>
    <td class="cellrowborder" valign="top" width="11.52%" headers="mcps1.2.6.1.2 "><p id="p2068716991615"><a name="p2068716991615"></a><a name="p2068716991615"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.75%" headers="mcps1.2.6.1.3 "><p id="p19687494167"><a name="p19687494167"></a><a name="p19687494167"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1268718918163"><a name="p1268718918163"></a><a name="p1268718918163"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.96999999999999%" headers="mcps1.2.6.1.5 "><p id="p1768709101615"><a name="p1768709101615"></a><a name="p1768709101615"></a>Specifies the matching times.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.LOB\_RESET

    This function clears a character string and resets the string to the value of  **value**.

    The function prototype of  **PKG\_UTIL.LOB\_RESET**  is as follows:

    ```
    PKG_UTIL.LOB_RESET(
    lob          INOUT   bytea,
    len          INOUT   int,
    start        IN   int DEFAUTL 1,
    value        IN   char default 0
    )
    RETURN record;
    ```

    **Table  8**  PKG\_UTIL.LOB\_RESET interface parameters

    <a name="table4614529112119"></a>
    <table><thead align="left"><tr id="row11614132914213"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p196141029162114"><a name="p196141029162114"></a><a name="p196141029162114"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p1961502992116"><a name="p1961502992116"></a><a name="p1961502992116"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p116152292217"><a name="p116152292217"></a><a name="p116152292217"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p3615529112110"><a name="p3615529112110"></a><a name="p3615529112110"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p11615929112112"><a name="p11615929112112"></a><a name="p11615929112112"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1861552918213"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p66152029192112"><a name="p66152029192112"></a><a name="p66152029192112"></a>lob</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p18615132913215"><a name="p18615132913215"></a><a name="p18615132913215"></a>bytea</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p76155296212"><a name="p76155296212"></a><a name="p76155296212"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p176154293214"><a name="p176154293214"></a><a name="p176154293214"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p11615329132111"><a name="p11615329132111"></a><a name="p11615329132111"></a>Indicates the character string for reset.</p>
    </td>
    </tr>
    <tr id="row161562942113"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p96153297217"><a name="p96153297217"></a><a name="p96153297217"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p861642916213"><a name="p861642916213"></a><a name="p861642916213"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p1761652982119"><a name="p1761652982119"></a><a name="p1761652982119"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p0616129192116"><a name="p0616129192116"></a><a name="p0616129192116"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p5616172913215"><a name="p5616172913215"></a><a name="p5616172913215"></a>Specifies the length of the string to be reset.</p>
    </td>
    </tr>
    <tr id="row5616529112117"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p10641203182416"><a name="p10641203182416"></a><a name="p10641203182416"></a>start</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p1461652918213"><a name="p1461652918213"></a><a name="p1461652918213"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p8616122919216"><a name="p8616122919216"></a><a name="p8616122919216"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p561622912115"><a name="p561622912115"></a><a name="p561622912115"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p12616132918211"><a name="p12616132918211"></a><a name="p12616132918211"></a>Specifies the start position for reset.</p>
    </td>
    </tr>
    <tr id="row1161622913215"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p96164291211"><a name="p96164291211"></a><a name="p96164291211"></a>value</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p18617142952111"><a name="p18617142952111"></a><a name="p18617142952111"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p1061792919215"><a name="p1061792919215"></a><a name="p1061792919215"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p1361752972118"><a name="p1361752972118"></a><a name="p1361752972118"></a>Yes</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p0617202917213"><a name="p0617202917213"></a><a name="p0617202917213"></a>Sets characters. Default value: <strong id="b939553474215"><a name="b939553474215"></a><a name="b939553474215"></a>'0'</strong></p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.IO\_PRINT

    This function outputs a string.

    The function prototype of  **PKG\_UTIL.IO\_PRINT**  is as follows:

    ```
    PKG_UTIL.IO_PRINT(
    format       IN   text,
    is_one_line  IN   boolean
    )
    RETURN void;
    ```

    **Table  9**  PKG\_UTIL.IO\_PRINT interface parameters

    <a name="table1257171516297"></a>
    <table><thead align="left"><tr id="row1525871582917"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p17258315192917"><a name="p17258315192917"></a><a name="p17258315192917"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p17258111519292"><a name="p17258111519292"></a><a name="p17258111519292"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p14258111511298"><a name="p14258111511298"></a><a name="p14258111511298"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p19258161522917"><a name="p19258161522917"></a><a name="p19258161522917"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p10258131572912"><a name="p10258131572912"></a><a name="p10258131572912"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row15258915162914"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p025851572910"><a name="p025851572910"></a><a name="p025851572910"></a>format</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p19258101532914"><a name="p19258101532914"></a><a name="p19258101532914"></a>text</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p1825891514294"><a name="p1825891514294"></a><a name="p1825891514294"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p14258191512910"><a name="p14258191512910"></a><a name="p14258191512910"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p19259171519294"><a name="p19259171519294"></a><a name="p19259171519294"></a>Indicates the character string to be output.</p>
    </td>
    </tr>
    <tr id="row152591615152912"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p2025917155291"><a name="p2025917155291"></a><a name="p2025917155291"></a>is_one_line</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p18259191513294"><a name="p18259191513294"></a><a name="p18259191513294"></a>boolean</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p52598151291"><a name="p52598151291"></a><a name="p52598151291"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p19259101562910"><a name="p19259101562910"></a><a name="p19259101562910"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p82591715192910"><a name="p82591715192910"></a><a name="p82591715192910"></a>Indicates whether to output the string as a line.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.RAW\_GET\_LENGTH

    This function obtains the length of  **RAW**  data.

    The function prototype of  **PKG\_UTIL.RAW\_GET\_LENGTH**  is as follows:

    ```
    PKG_UTIL.RAW_GET_LENGTH(
    value       IN   raw
    )
    RETURN integer;
    ```

    **Table  10**  PKG\_UTIL.RAW\_GET\_LENGTH interface parameters

    <a name="table761918355917"></a>
    <table><thead align="left"><tr id="row961915351498"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p1161983511919"><a name="p1161983511919"></a><a name="p1161983511919"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p106191935092"><a name="p106191935092"></a><a name="p106191935092"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p46205353919"><a name="p46205353919"></a><a name="p46205353919"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p86200351398"><a name="p86200351398"></a><a name="p86200351398"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p14620735895"><a name="p14620735895"></a><a name="p14620735895"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row2620635393"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p106207351395"><a name="p106207351395"></a><a name="p106207351395"></a>raw</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p4620435990"><a name="p4620435990"></a><a name="p4620435990"></a>raw</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p16620123520910"><a name="p16620123520910"></a><a name="p16620123520910"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p6620835796"><a name="p6620835796"></a><a name="p6620835796"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p196201835695"><a name="p196201835695"></a><a name="p196201835695"></a>Indicates the object whose length is to be obtained.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.RAW\_CAST\_FROM\_VARCHAR2

    This function converts  **VARCHAR2**  data to  **RAW**  data.

    The function prototype of  **PKG\_UTIL.RAW\_CAST\_FROM\_VARCHAR2**  is as follows:

    ```
    PKG_UTIL.RAW_CAST_FROM_VARCHAR2(
    str       IN   varchar2
    )
    RETURN raw;
    ```

    **Table  11**  PKG\_UTIL.RAW\_CAST\_FROM\_VARCHAR2 interface parameters

    <a name="table220317411392"></a>
    <table><thead align="left"><tr id="row7204841595"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p112041415913"><a name="p112041415913"></a><a name="p112041415913"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="9.33%" id="mcps1.2.6.1.2"><p id="p12048411591"><a name="p12048411591"></a><a name="p12048411591"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="12.940000000000001%" id="mcps1.2.6.1.3"><p id="p132044411892"><a name="p132044411892"></a><a name="p132044411892"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p52047411495"><a name="p52047411495"></a><a name="p52047411495"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p152046418916"><a name="p152046418916"></a><a name="p152046418916"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row10204841496"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p220484118915"><a name="p220484118915"></a><a name="p220484118915"></a>str</p>
    </td>
    <td class="cellrowborder" valign="top" width="9.33%" headers="mcps1.2.6.1.2 "><p id="p620424114919"><a name="p620424114919"></a><a name="p620424114919"></a>varchar2</p>
    </td>
    <td class="cellrowborder" valign="top" width="12.940000000000001%" headers="mcps1.2.6.1.3 "><p id="p1220412411590"><a name="p1220412411590"></a><a name="p1220412411590"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p132043411898"><a name="p132043411898"></a><a name="p132043411898"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p9205741399"><a name="p9205741399"></a><a name="p9205741399"></a>Indicates the source data to be converted</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.CAST\_FROM\_BINARY\_INTEGER

    This function converts binary integers to  **RAW**  data.

    The function prototype of  **PKG\_UTIL.CAST\_FROM\_BINARY\_INTEGER**  is as follows:

    ```
    PKG_UTIL.CAST_FROM_BINARY_INTEGER(
    value       IN   integer,
    endianess   IN   integer
    )
    RETURN raw;
    ```

    **Table  12**  PKG\_UTIL.CAST\_FROM\_BINARY\_INTEGER interface parameters

    <a name="table121257431917"></a>
    <table><thead align="left"><tr id="row4126104317912"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p61263431296"><a name="p61263431296"></a><a name="p61263431296"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p16126164318918"><a name="p16126164318918"></a><a name="p16126164318918"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p1412684310917"><a name="p1412684310917"></a><a name="p1412684310917"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p0126134318917"><a name="p0126134318917"></a><a name="p0126134318917"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p121262431998"><a name="p121262431998"></a><a name="p121262431998"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1012612438917"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p19126184314911"><a name="p19126184314911"></a><a name="p19126184314911"></a>value</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p2126154318911"><a name="p2126154318911"></a><a name="p2126154318911"></a>integer</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p1512613436919"><a name="p1512613436919"></a><a name="p1512613436919"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p131272431194"><a name="p131272431194"></a><a name="p131272431194"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p8127643097"><a name="p8127643097"></a><a name="p8127643097"></a>Indicates the source data that needs to be converted into <strong id="b14511932436"><a name="b14511932436"></a><a name="b14511932436"></a>RAW</strong> data</p>
    </td>
    </tr>
    <tr id="row312715437914"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p9127114311913"><a name="p9127114311913"></a><a name="p9127114311913"></a>endianess</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p3127184313914"><a name="p3127184313914"></a><a name="p3127184313914"></a>integer</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p101278435912"><a name="p101278435912"></a><a name="p101278435912"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p912713431691"><a name="p912713431691"></a><a name="p912713431691"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p1012714431991"><a name="p1012714431991"></a><a name="p1012714431991"></a>Specifies the <strong id="b12241115164410"><a name="b12241115164410"></a><a name="b12241115164410"></a>INTEGER</strong> value <strong id="b8242851194416"><a name="b8242851194416"></a><a name="b8242851194416"></a>1</strong> or <strong id="b192448514447"><a name="b192448514447"></a><a name="b192448514447"></a>2</strong> for the byte sequence. (<strong id="b2245051134419"><a name="b2245051134419"></a><a name="b2245051134419"></a>1</strong> indicates <strong id="b724795111440"><a name="b724795111440"></a><a name="b724795111440"></a>BIG_ENDIAN</strong> and <strong id="b7249125118449"><a name="b7249125118449"></a><a name="b7249125118449"></a>2</strong> indicates <strong id="b192501251164412"><a name="b192501251164412"></a><a name="b192501251164412"></a>LITTLE-ENDIAN</strong>.)</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.CAST\_TO\_BINARY\_INTEGER

    This function converts  **RAW**  data into binary integers.

    The function prototype of  **PKG\_UTIL.CAST\_TO\_BINARY\_INTEGER**  is as follows:

    ```
    PKG_UTIL.CAST_TO_BINARY_INTEGER(
    value       IN   raw,
    endianess   IN   integer
    )
    RETURN integer;
    ```

    **Table  13**  PKG\_UTIL.CAST\_TO\_BINARY\_INTEGER interface parameters

    <a name="table10591144314159"></a>
    <table><thead align="left"><tr id="row7592134391511"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p1359217433157"><a name="p1359217433157"></a><a name="p1359217433157"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p12592164311511"><a name="p12592164311511"></a><a name="p12592164311511"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p35921643181513"><a name="p35921643181513"></a><a name="p35921643181513"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p12592184371513"><a name="p12592184371513"></a><a name="p12592184371513"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p65921943121515"><a name="p65921943121515"></a><a name="p65921943121515"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1859284391512"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p459212433152"><a name="p459212433152"></a><a name="p459212433152"></a>value</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p14592204351519"><a name="p14592204351519"></a><a name="p14592204351519"></a>raw</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p3592174310155"><a name="p3592174310155"></a><a name="p3592174310155"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p12592174317158"><a name="p12592174317158"></a><a name="p12592174317158"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p135931243191517"><a name="p135931243191517"></a><a name="p135931243191517"></a>Indicates the source <strong id="b165683332413"><a name="b165683332413"></a><a name="b165683332413"></a>RAW</strong> data that needs to be converted into binary integers.</p>
    </td>
    </tr>
    <tr id="row11593144313157"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p35931843111514"><a name="p35931843111514"></a><a name="p35931843111514"></a>endianess</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p05939431152"><a name="p05939431152"></a><a name="p05939431152"></a>integer</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p155931543151512"><a name="p155931543151512"></a><a name="p155931543151512"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p95931433159"><a name="p95931433159"></a><a name="p95931433159"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="en-us_topic_0059779261_a49c0d065c5bd460c8dc0d35af62c5062"><a name="en-us_topic_0059779261_a49c0d065c5bd460c8dc0d35af62c5062"></a><a name="en-us_topic_0059779261_a49c0d065c5bd460c8dc0d35af62c5062"></a>Specifies the <strong id="b1279817247454"><a name="b1279817247454"></a><a name="b1279817247454"></a>INTEGER</strong> value <strong id="b1280419249453"><a name="b1280419249453"></a><a name="b1280419249453"></a>1</strong> or <strong id="b6806112410456"><a name="b6806112410456"></a><a name="b6806112410456"></a>2</strong> for the byte sequence. (<strong id="b28081324154518"><a name="b28081324154518"></a><a name="b28081324154518"></a>1</strong> indicates <strong id="b168099243453"><a name="b168099243453"></a><a name="b168099243453"></a>BIG_ENDIAN</strong> and <strong id="b28117246452"><a name="b28117246452"></a><a name="b28117246452"></a>2</strong> indicates <strong id="b7813724104520"><a name="b7813724104520"></a><a name="b7813724104520"></a>LITTLE-ENDIAN</strong>.)</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.SET\_RANDOM\_SEED

    This function sets a random seed.

    The function prototype of  **PKG\_UTIL.SET\_RANDOM\_SEED**  is as follows:

    ```
    PKG_UTIL.RANDOM_SET_SEED(
    seed         IN   int
    )
    RETURN integer;
    ```

    **Table  14**  PKG\_UTIL.SET\_RANDOM\_SEED interface parameters

    <a name="table19819158518"></a>
    <table><thead align="left"><tr id="row178117153511"><th class="cellrowborder" valign="top" width="14.050000000000004%" id="mcps1.2.6.1.1"><p id="p118114158518"><a name="p118114158518"></a><a name="p118114158518"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="8.480000000000002%" id="mcps1.2.6.1.2"><p id="p98151505114"><a name="p98151505114"></a><a name="p98151505114"></a>Type</p>
    </th>
    <th class="cellrowborder" valign="top" width="13.790000000000003%" id="mcps1.2.6.1.3"><p id="p981915145110"><a name="p981915145110"></a><a name="p981915145110"></a>Input/Output Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="10.71%" id="mcps1.2.6.1.4"><p id="p1281915165111"><a name="p1281915165111"></a><a name="p1281915165111"></a>Can Be Empty</p>
    </th>
    <th class="cellrowborder" valign="top" width="52.970000000000006%" id="mcps1.2.6.1.5"><p id="p1782111545112"><a name="p1782111545112"></a><a name="p1782111545112"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row11821115165117"><td class="cellrowborder" valign="top" width="14.050000000000004%" headers="mcps1.2.6.1.1 "><p id="p1182415105112"><a name="p1182415105112"></a><a name="p1182415105112"></a>seed</p>
    </td>
    <td class="cellrowborder" valign="top" width="8.480000000000002%" headers="mcps1.2.6.1.2 "><p id="p68241515513"><a name="p68241515513"></a><a name="p68241515513"></a>int</p>
    </td>
    <td class="cellrowborder" valign="top" width="13.790000000000003%" headers="mcps1.2.6.1.3 "><p id="p1182181525114"><a name="p1182181525114"></a><a name="p1182181525114"></a>IN</p>
    </td>
    <td class="cellrowborder" valign="top" width="10.71%" headers="mcps1.2.6.1.4 "><p id="p138211158510"><a name="p138211158510"></a><a name="p138211158510"></a>No</p>
    </td>
    <td class="cellrowborder" valign="top" width="52.970000000000006%" headers="mcps1.2.6.1.5 "><p id="p1502188017"><a name="p1502188017"></a><a name="p1502188017"></a>Sets a random seed.</p>
    </td>
    </tr>
    </tbody>
    </table>


-   PKG\_UTIL.RANDOM\_GET\_VALUE

    This function returns a random number ranging from 0 to 1.

    The function prototype of  **PKG\_UTIL.RANDOM\_GET\_VALUE**  is as follows:

    ```
    PKG_UTIL.RANDOM_GET_VALUE(
    )
    RETURN numeric;
    ```


-   PKG\_UTIL.FILE\_SET\_DIRNAME

    This function sets the directory to be operated. It must be called to set directory for each operation involving a single directory.

    The prototype of the  **PKG\_UTIL.FILE\_SET\_DIRNAME**  function is as follows:

    ```
    PKG_UTIL.FILE_SET_DIRNAME(
    dir  IN  text
    )
    RETURN bool
    ```

    **Table  15**  PKG\_UTIL.FILE\_SET\_DIRNAME interface parameters

    <a name="table166848441426"></a>
    <table><thead align="left"><tr id="row5684114404213"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p186841344114210"><a name="p186841344114210"></a><a name="p186841344114210"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p36841441422"><a name="p36841441422"></a><a name="p36841441422"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row20684644144217"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p136841644174217"><a name="p136841644174217"></a><a name="p136841644174217"></a>dirname</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p26851444144218"><a name="p26851444144218"></a><a name="p26851444144218"></a>Directory of a file. It is a string, indicating an object name.</p>
    <div class="note" id="en-us_topic_0059778749_n78d8bb15df7d4c44ba9c1ebc721fa816"><a name="en-us_topic_0059778749_n78d8bb15df7d4c44ba9c1ebc721fa816"></a><a name="en-us_topic_0059778749_n78d8bb15df7d4c44ba9c1ebc721fa816"></a><span class="notetitle"> NOTE: </span><div class="notebody"><p id="p8726203451617"><a name="p8726203451617"></a><a name="p8726203451617"></a>File directories need to be added to the system catalog <strong id="b1731475016164"><a name="b1731475016164"></a><a name="b1731475016164"></a><a href="en-us_topic_0289900041.md">PG_DIRECTORY</a></strong>. If the input path does not match the path in <strong id="b18314175015162"><a name="b18314175015162"></a><a name="b18314175015162"></a><a href="en-us_topic_0289900041.md">PG_DIRECTORY</a></strong>, an error indicating that the path does not exist will be reported. Functions that involve <strong id="b2315165071619"><a name="b2315165071619"></a><a name="b2315165071619"></a>location</strong> as parameters also comply with this rule.</p>
    </div></div>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_OPEN

    This function opens a file. A maximum of 50 files can be opened at a time. This function returns a handle of the  **INTEGER**  type.

    The prototype of the  **PKG\_UTIL.FILE\_OPEN**  function is as follows:

    ```
    PKG_UTIL.FILE_OPEN(
    file_name    IN  text, 
    open_mode    IN  integer)
    ```

    **Table  16**  PKG\_UTIL.FILE\_OPEN interface parameters

    <a name="table19482123794210"></a>
    <table><thead align="left"><tr id="row6483537174213"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p548313754218"><a name="p548313754218"></a><a name="p548313754218"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p3483103754211"><a name="p3483103754211"></a><a name="p3483103754211"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row12483173718429"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p14831037164211"><a name="p14831037164211"></a><a name="p14831037164211"></a>file_name</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1721362814236"><a name="p1721362814236"></a><a name="p1721362814236"></a>File name with an extension (file type), excluding the path name. A path contained in a file name is ignored in the <strong id="b1993319537500"><a name="b1993319537500"></a><a name="b1993319537500"></a>OPEN</strong> function. In Unix, the file name cannot end with the combination of a slash and a dot (/.).</p>
    </td>
    </tr>
    <tr id="row16179205510259"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1418014559254"><a name="p1418014559254"></a><a name="p1418014559254"></a>open_mode</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p52148283234"><a name="p52148283234"></a><a name="p52148283234"></a>File opening mode, including <strong id="b66167018514"><a name="b66167018514"></a><a name="b66167018514"></a>r</strong> (read), <strong id="b662220175111"><a name="b662220175111"></a><a name="b662220175111"></a>w</strong> (write), and <strong id="b18622110185112"><a name="b18622110185112"></a><a name="b18622110185112"></a>a</strong> (append).</p>
    <div class="note" id="note0112230123619"><a name="note0112230123619"></a><a name="note0112230123619"></a><span class="notetitle"> NOTE: </span><div class="notebody"><p id="p811303012363"><a name="p811303012363"></a><a name="p811303012363"></a>For the write operation, the system checks the file type. If the ELF file is written, an error is reported and the system exits.</p>
    </div></div>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_SET\_MAX\_LINE\_SIZE

    This function sets the maximum length of a line to be written to a file.

    The function prototype of  **PKG\_UTIL.FILE\_SET\_MAX\_LINE\_SIZE**  is as follows:

    ```
    PKG_UTIL.FILE_SET_MAX_LINE_SIZE(
    max_line_size in integer)
    RETURN BOOL
    ```

    **Table  17**  PKG\_UTIL.FILE\_SET\_MAX\_LINE\_SIZE interface parameters

    <a name="table61151733184216"></a>
    <table><thead align="left"><tr id="row011514335426"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p13116143314214"><a name="p13116143314214"></a><a name="p13116143314214"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p5116113318422"><a name="p5116113318422"></a><a name="p5116113318422"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row10116163314427"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p119038172817"><a name="p119038172817"></a><a name="p119038172817"></a>max_line_size</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p121161233144216"><a name="p121161233144216"></a><a name="p121161233144216"></a>Maximum number of characters in each line, including newline characters. The minimum value is <strong id="b5364144105112"><a name="b5364144105112"></a><a name="b5364144105112"></a>1</strong> and the maximum is <strong id="b7365124418515"><a name="b7365124418515"></a><a name="b7365124418515"></a>32767</strong>. If this parameter is not specified, the default value <strong id="b95288171935"><a name="b95288171935"></a><a name="b95288171935"></a>1024</strong> is used.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_IS\_CLOSE

    This function checks whether a file handle is closed.

    The prototype of the  **PKG\_UTIL.FILE\_IS\_CLOSE**  function is as follows:

    ```
    PKG_UTIL.FILE_IS_CLOSE(
    file in integer
    )
    RETURN BOOL
    ```

    **Table  18**  PKG\_UTIL.FILE\_IS\_CLOSE interface parameters

    <a name="table19150192784217"></a>
    <table><thead align="left"><tr id="row9151162719422"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p17151527164220"><a name="p17151527164220"></a><a name="p17151527164220"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p4151132734216"><a name="p4151132734216"></a><a name="p4151132734216"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row715142754219"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p6151192734219"><a name="p6151192734219"></a><a name="p6151192734219"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p58820301919"><a name="p58820301919"></a><a name="p58820301919"></a>Opened file handle</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_READ

    This function reads a line of data from an open file handle based on the specified length.

    The prototype of the  **PKG\_UTIL.FILE\_READ**  function is as follows:

    ```
    PKG_UTIL.FILE_READ(
    file     IN   integer,
    buffer   OUT  text,
    len      IN   integer)
    ```

    **Table  19**  PKG\_UTIL.FILE\_READ interface parameters

    <a name="table197152044212"></a>
    <table><thead align="left"><tr id="row697113203428"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p4971720134217"><a name="p4971720134217"></a><a name="p4971720134217"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p1097152018427"><a name="p1097152018427"></a><a name="p1097152018427"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row179711320134219"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p109711620164217"><a name="p109711620164217"></a><a name="p109711620164217"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p13579172020159"><a name="p13579172020159"></a><a name="p13579172020159"></a>File handle opened by calling the <strong id="b10778981701"><a name="b10778981701"></a><a name="b10778981701"></a>OPEN</strong> function. The file must be opened in read mode. Otherwise, the <strong id="b3779481007"><a name="b3779481007"></a><a name="b3779481007"></a>INVALID_OPERATION</strong> exception is thrown.</p>
    </td>
    </tr>
    <tr id="row2456230111316"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1245620305134"><a name="p1245620305134"></a><a name="p1245620305134"></a>buffer</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p8216152813238"><a name="p8216152813238"></a><a name="p8216152813238"></a>Buffer used to receive data</p>
    </td>
    </tr>
    <tr id="row15547172751318"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p954814274132"><a name="p954814274132"></a><a name="p954814274132"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1554812751314"><a name="p1554812751314"></a><a name="p1554812751314"></a>Number of bytes read from a file</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_READLINE

    This function reads a line of data from an open file handle based on the specified length.

    The prototype of the  **PKG\_UTIL.FILE\_READLINE**  function is as follows:

    ```
    PKG_UTIL.FILE_READLINE(
    file    IN  integer,
    buffer  OUT text,
    len     IN  integer default 1024)
    
    ```

    **Table  20**  PKG\_UTIL.FILE\_READLINE interface parameters

    <a name="table12516161417429"></a>
    <table><thead align="left"><tr id="row351711142422"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p15517614124219"><a name="p15517614124219"></a><a name="p15517614124219"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p185173147427"><a name="p185173147427"></a><a name="p185173147427"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row951731444210"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p751716149424"><a name="p751716149424"></a><a name="p751716149424"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1121642852315"><a name="p1121642852315"></a><a name="p1121642852315"></a>File handle opened by calling the <strong id="b901454026"><a name="b901454026"></a><a name="b901454026"></a>OPEN</strong> function. The file must be opened in read mode. Otherwise, the <strong id="b356454054"><a name="b356454054"></a><a name="b356454054"></a>INVALID_OPERATION</strong> exception is thrown.</p>
    </td>
    </tr>
    <tr id="row6816363146"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1481617611417"><a name="p1481617611417"></a><a name="p1481617611417"></a>buffer</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1081616181414"><a name="p1081616181414"></a><a name="p1081616181414"></a>Buffer used to receive data</p>
    </td>
    </tr>
    <tr id="row1251991291418"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1651991218149"><a name="p1651991218149"></a><a name="p1651991218149"></a>len</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p15519712191420"><a name="p15519712191420"></a><a name="p15519712191420"></a>Number of bytes read from a file. The default value is <strong id="b1291513589511"><a name="b1291513589511"></a><a name="b1291513589511"></a>NULL</strong>. If the default value <strong id="b2593011769"><a name="b2593011769"></a><a name="b2593011769"></a>NULL</strong> is used, <strong id="b15941319617"><a name="b15941319617"></a><a name="b15941319617"></a>max_line_size</strong> is used to specify the line size.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_WRITE

    This function writes the data specified in the buffer to a file.

    The prototype of the  **PKG\_UTIL.FILE\_WRITE**  function is as follows:

    ```
    PKG_UTIL.FILE_WRITE(
    file in integer,
    buffer in text
    )
    RETURN BOOL
    ```

    **Table  21**  PKG\_UTIL.FILE\_WRITE interface parameters

    <a name="table143061674425"></a>
    <table><thead align="left"><tr id="row1530611720424"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1430620715423"><a name="p1430620715423"></a><a name="p1430620715423"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p930667114213"><a name="p930667114213"></a><a name="p930667114213"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row430667104215"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p163061978428"><a name="p163061978428"></a><a name="p163061978428"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p122201528112319"><a name="p122201528112319"></a><a name="p122201528112319"></a>Opened file handle</p>
    </td>
    </tr>
    <tr id="row161235525101"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p131237528103"><a name="p131237528103"></a><a name="p131237528103"></a>buffer</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p922012282232"><a name="p922012282232"></a><a name="p922012282232"></a>Text data to be written to a file. The maximum buffer size is 32767 bytes. If no value is specified, the default value is 1024 bytes. Before the writing is performed, the buffer occupied by <strong id="b78109522618"><a name="b78109522618"></a><a name="b78109522618"></a>PUT</strong> operations cannot exceed 32767 bytes.</p>
    <div class="note" id="note179122293811"><a name="note179122293811"></a><a name="note179122293811"></a><span class="notetitle"> NOTE: </span><div class="notebody"><p id="p179115228384"><a name="p179115228384"></a><a name="p179115228384"></a>For the write operation, the system checks the file type. If the ELF file is written, an error is reported and the system exits.</p>
    </div></div>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_NEWLINE

    This function writes a line terminator to an open file. The line terminator is related to the platform.

    The prototype of the  **PKG\_UTIL.FILE\_NEWLINE**  function is as follows:

    ```
    PKG_UTIL.FILE_NEWLINE(
    file in integer
    )
    RETURN BOOL
    ```

    **Table  22**  PKG\_UTIL.FILE\_NEWLINE interface parameters

    <a name="table192311194217"></a>
    <table><thead align="left"><tr id="row192314116422"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p42316134215"><a name="p42316134215"></a><a name="p42316134215"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p172311316421"><a name="p172311316421"></a><a name="p172311316421"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row223151144210"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1123111104210"><a name="p1123111104210"></a><a name="p1123111104210"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p172411327669"><a name="p172411327669"></a><a name="p172411327669"></a>Opened file handle</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_WRITELINE

    Writes a line to a file.

    The prototype of the  **PKG\_UTIL.FILE\_WRITELINE**  function is as follows:

    ```
    PKG_UTIL.FILE_WRITELINE(
    file in integer,
    buffer in text,
    flush in bool default false
    )
    RETURN BOOL
    ```

    **Table  23**  PKG\_UTIL.FILE\_WRITELINE parameters

    <a name="table060195494918"></a>
    <table><thead align="left"><tr id="row1460195419496"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p146075444911"><a name="p146075444911"></a><a name="p146075444911"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p12601754144911"><a name="p12601754144911"></a><a name="p12601754144911"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row11601054164917"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1961195444917"><a name="p1961195444917"></a><a name="p1961195444917"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p3612054144910"><a name="p3612054144910"></a><a name="p3612054144910"></a>Opened file handle.</p>
    </td>
    </tr>
    <tr id="row18682155235518"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p126834528557"><a name="p126834528557"></a><a name="p126834528557"></a>buffer</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p147885149563"><a name="p147885149563"></a><a name="p147885149563"></a>Content to be written.</p>
    </td>
    </tr>
    <tr id="row0258105655514"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p725955665513"><a name="p725955665513"></a><a name="p725955665513"></a>flush</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p625925615554"><a name="p625925615554"></a><a name="p625925615554"></a>Whether to flush data to disks.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_READ\_RAW

    This function reads binary data of a specified length from an open file handle and returns the read binary data. The return type is  **raw**.

    The prototype of the  **PKG\_UTIL.FILE\_READ\_RAW**  function is as follows:

    ```
    PKG_UTIL.FILE_READ_RAW(
    file      in integer,
    length    in integer default NULL
    )
    RETURN raw
    ```

    **Table  24**  PKG\_UTIL.FILE\_READ\_RAW interface parameters

    <a name="table3611164110197"></a>
    <table><thead align="left"><tr id="row66123419192"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1661216417196"><a name="p1661216417196"></a><a name="p1661216417196"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p76121141131918"><a name="p76121141131918"></a><a name="p76121141131918"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row9612154161914"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p176121141121910"><a name="p176121141121910"></a><a name="p176121141121910"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1461234120198"><a name="p1461234120198"></a><a name="p1461234120198"></a>Opened file handle</p>
    </td>
    </tr>
    <tr id="row96121941131916"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p196121241101910"><a name="p196121241101910"></a><a name="p196121241101910"></a>length</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p5612141151914"><a name="p5612141151914"></a><a name="p5612141151914"></a>Length of the data to be read. The default value is <strong id="b20546133515293"><a name="b20546133515293"></a><a name="b20546133515293"></a>NULL</strong>. By default, all data in the file is read. The maximum size is 1 GB.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_WRITE\_RAW

    This function writes the input binary object  **RAW**  to an opened file. If the insertion is successful,  **true**  is returned.

    The prototype of the  **PKG\_UTIL.FILE\_WRITE\_RAW**  function is as follows:

    ```
    PKG_UTIL.FILE_WRITE_RAW(
    file in integer,
    r    in raw
    )
    RETURN BOOL
    ```

    **Table  25**  PKG\_UTIL.FILE\_NEWLINE interface parameters

    <a name="table23691620201119"></a>
    <table><thead align="left"><tr id="row18370172013117"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1237062041115"><a name="p1237062041115"></a><a name="p1237062041115"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p93701420181112"><a name="p93701420181112"></a><a name="p93701420181112"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row8370162010111"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p12370820191117"><a name="p12370820191117"></a><a name="p12370820191117"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p53706208115"><a name="p53706208115"></a><a name="p53706208115"></a>Opened file handle</p>
    </td>
    </tr>
    <tr id="row7518123052214"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p185191130152213"><a name="p185191130152213"></a><a name="p185191130152213"></a>r</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p95195301222"><a name="p95195301222"></a><a name="p95195301222"></a>Data to be written to the file</p>
    <div class="note" id="note45341742153815"><a name="note45341742153815"></a><a name="note45341742153815"></a><span class="notetitle"> NOTE: </span><div class="notebody"><p id="p1453594216386"><a name="p1453594216386"></a><a name="p1453594216386"></a>For the write operation, the system checks the file type. If the ELF file is written, an error is reported and the system exits.</p>
    </div></div>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_FLUSH

    Data in a file handle must be written into a physical file. Data in the buffer must have a line terminator. Refresh is important if a file must be read when it is opened. For example, debugging information can be refreshed to a file so that it can be read immediately.

    The prototype of the  **PKG\_UTIL.FILE\_FLUSH**  function is as follows:

    ```
    PKG_UTIL.FILE_FLUSH (
    file in integer
    )
    RETURN VOID
    ```

    **Table  26**  PKG\_UTIL.FILE\_FLUSH interface parameters

    <a name="table1660795544114"></a>
    <table><thead align="left"><tr id="row11607125534118"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1607355134110"><a name="p1607355134110"></a><a name="p1607355134110"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p560710558411"><a name="p560710558411"></a><a name="p560710558411"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row76074559415"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p16607655104120"><a name="p16607655104120"></a><a name="p16607655104120"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p143191224054"><a name="p143191224054"></a><a name="p143191224054"></a>Opened file handle</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_CLOSE

    This function closes an open file handle.

    The prototype of the  **PKG\_UTIL.FILE\_CLOSE**  function is as follows:

    ```
    PKG_UTIL.FILE_CLOSE (
    file in integer
    )
    RETURN BOOL
    ```

    **Table  27**  PKG\_UTIL.FILE\_CLOSE interface parameters

    <a name="table78535463415"></a>
    <table><thead align="left"><tr id="row17853194614111"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p9853046184114"><a name="p9853046184114"></a><a name="p9853046184114"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p8853124694115"><a name="p8853124694115"></a><a name="p8853124694115"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row3853946164112"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p15853124618414"><a name="p15853124618414"></a><a name="p15853124618414"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p9853124674110"><a name="p9853124674110"></a><a name="p9853124674110"></a>Opened file handle</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_REMOVE

    This function deletes a disk file. To perform this operation, you must have required permissions.

    The prototype of the  **PKG\_UTIL.FILE\_REMOVE**  function is as follows:

    ```
    PKG_UTIL.FILE_REMOVE(
    file_name in text
    )
    RETURN VOID 
    ```

    **Table  28**  PKG\_UTIL.FILE\_REMOVE interface parameters

    <a name="table5813940114117"></a>
    <table><thead align="left"><tr id="row88131440204117"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1181312404414"><a name="p1181312404414"></a><a name="p1181312404414"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p881311406415"><a name="p881311406415"></a><a name="p881311406415"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row5813140124115"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p16813940164111"><a name="p16813940164111"></a><a name="p16813940164111"></a>filen_ame</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p137013571635"><a name="p137013571635"></a><a name="p137013571635"></a>Name of the file to be deleted</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_RENAME

    The function renames a file on the disk, similar to  **mv**  in Unix.

    The prototype of the  **PKG\_UTIL.FILE\_RENAME**  function is as follows:

    ```
    PKG_UTIL.FILE_RENAME(
    text src_dir in text, 
    text src_file_name in text, 
    text dest_dir in text, 
    text dest_file_name in text, 
     overwrite boolean default false)
    ```

    **Table  29**  PKG\_UTIL.FILE\_RENAME interface parameters

    <a name="table16448929134114"></a>
    <table><thead align="left"><tr id="row1644918299415"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p174491529134119"><a name="p174491529134119"></a><a name="p174491529134119"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p7449152917410"><a name="p7449152917410"></a><a name="p7449152917410"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row244962924116"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1356616304115"><a name="p1356616304115"></a><a name="p1356616304115"></a>src_dir</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p14492029144119"><a name="p14492029144119"></a><a name="p14492029144119"></a>Source file directory (case-sensitive)</p>
    </td>
    </tr>
    <tr id="row2077782715115"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p2068873211114"><a name="p2068873211114"></a><a name="p2068873211114"></a>src_file_name</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p3777027916"><a name="p3777027916"></a><a name="p3777027916"></a>Source file name</p>
    </td>
    </tr>
    <tr id="row1750162419111"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p177211734314"><a name="p177211734314"></a><a name="p177211734314"></a>dest_dir</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p13500241613"><a name="p13500241613"></a><a name="p13500241613"></a>Target file directory (case-sensitive)</p>
    </td>
    </tr>
    <tr id="row1890519198118"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1342911371016"><a name="p1342911371016"></a><a name="p1342911371016"></a>dest_file_name</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p79053192111"><a name="p79053192111"></a><a name="p79053192111"></a>Target file name</p>
    </td>
    </tr>
    <tr id="row5350316511"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p19575740318"><a name="p19575740318"></a><a name="p19575740318"></a>overwrite</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1351161615116"><a name="p1351161615116"></a><a name="p1351161615116"></a>The default value is <strong id="b17228104411108"><a name="b17228104411108"></a><a name="b17228104411108"></a>false</strong>. If a file with the same name exists in the destination directory, the file will not be rewritten.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_SIZE

    This function returns the size of a specified file.

    The prototype of the  **PKG\_UTIL.FILE\_SIZE**  function is as follows:

    ```
    bigint PKG_UTIL.FILE_SIZE(
    file_name in text
    )
    ```

    **Table  30**  PKG\_UTIL.FILE\_SIZE interface parameters

    <a name="table1311717232412"></a>
    <table><thead align="left"><tr id="row1011712324116"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p111732318415"><a name="p111732318415"></a><a name="p111732318415"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p111171723194115"><a name="p111171723194115"></a><a name="p111171723194115"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row12117142313417"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p011710231411"><a name="p011710231411"></a><a name="p011710231411"></a>file_name</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1928351210015"><a name="p1928351210015"></a><a name="p1928351210015"></a>File name</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_BLOCK\_SIZE

    This function returns the number of blocks contained in a specified file.

    The prototype of the  **PKG\_UTIL.FILE\_BLOCK\_SIZE**  function is as follows:

    ```
    bigint PKG_UTIL.FILE_BLOCK_SIZE(
    file_name in text
    )
    ```

    **Table  31**  PKG\_UTIL.FILE\_BLOCK\_SIZE interface parameters

    <a name="table637991316418"></a>
    <table><thead align="left"><tr id="row103799138419"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p143791132414"><a name="p143791132414"></a><a name="p143791132414"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p1137912136418"><a name="p1137912136418"></a><a name="p1137912136418"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row11379141317418"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1137914131415"><a name="p1137914131415"></a><a name="p1137914131415"></a>file_name</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1973403395910"><a name="p1973403395910"></a><a name="p1973403395910"></a>File name</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_EXISTS

    This function checks whether a file exists.

    The prototype of the  **PKG\_UTIL.FILE\_EXISTS**  function is as follows:

    ```
    PKG_UTIL.FILE_EXISTS(
    file_name in text
    )
    RETURN BOOL
    ```

    **Table  32**  PKG\_UTIL.FILE\_EXISTS interface parameters

    <a name="table672136184113"></a>
    <table><thead align="left"><tr id="row147211462413"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p197219614118"><a name="p197219614118"></a><a name="p197219614118"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p7721136124118"><a name="p7721136124118"></a><a name="p7721136124118"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row167212614412"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p272136104114"><a name="p272136104114"></a><a name="p272136104114"></a>file_name</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p319311413578"><a name="p319311413578"></a><a name="p319311413578"></a>File name</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_GETPOS

    This function specifies the offset of a returned file, in bytes.

    The prototype of the  **PKG\_UTIL.FILE\_GETPOS**  function is as follows:

    ```
    PKG_UTIL.FILE_GETPOS(
    file in integer 
    )
    RETURN BIGINT
    ```

    **Table  33**  PKG\_UTIL.FILE\_GETPOS interface parameters

    <a name="table0445504417"></a>
    <table><thead align="left"><tr id="row3445140114112"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p94461506412"><a name="p94461506412"></a><a name="p94461506412"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p54467010414"><a name="p54467010414"></a><a name="p54467010414"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row124461902414"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p104461507411"><a name="p104461507411"></a><a name="p104461507411"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p319874718158"><a name="p319874718158"></a><a name="p319874718158"></a>Opened file handle</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_SEEK

    This function adjusts the position of a file pointer forward or backward based on the specified number of bytes.

    The prototype of the  **PKG\_UTIL.FILE\_SEEK**  function is as follows:

    ```
    void PKG_UTIL.FILE_SEEK(
    file in integer,
    start in bigint default null
    )
    RETURN VOID
    ```

    **Table  34**  PKG\_UTIL.FILE\_SEEK interface parameters

    <a name="table16153123110407"></a>
    <table><thead align="left"><tr id="row7153113144012"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p8153163164012"><a name="p8153163164012"></a><a name="p8153163164012"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p1315383114402"><a name="p1315383114402"></a><a name="p1315383114402"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1315353114011"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p18459502406"><a name="p18459502406"></a><a name="p18459502406"></a>file</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p523902862314"><a name="p523902862314"></a><a name="p523902862314"></a>Opened file handle</p>
    </td>
    </tr>
    <tr id="row63201222105218"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p3320222115216"><a name="p3320222115216"></a><a name="p3320222115216"></a>start</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1632032211528"><a name="p1632032211528"></a><a name="p1632032211528"></a>File offset, in bytes.</p>
    </td>
    </tr>
    </tbody>
    </table>

-   PKG\_UTIL.FILE\_CLOSE\_ALL

    This function closes all file handles opened in a session.

    The prototype of the  **PKG\_UTIL.FILE\_CLOSE\_ALL**  function is as follows:

    ```
    PKG_UTIL.FILE_CLOSE_ALL(
    )
    RETURN VOID
    ```

    **Table  35**  PKG\_UTIL.FILE\_CLOSE\_ALL interface parameters

    <a name="table16284144010513"></a>
    <table><thead align="left"><tr id="row928484095110"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p6284540165110"><a name="p6284540165110"></a><a name="p6284540165110"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p928494055111"><a name="p928494055111"></a><a name="p928494055111"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row728412409513"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p3284740205116"><a name="p3284740205116"></a><a name="p3284740205116"></a>None</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p328454010515"><a name="p328454010515"></a><a name="p328454010515"></a>None</p>
    </td>
    </tr>
    </tbody>
    </table>


-   PKG\_UTIL.EXCEPTION\_REPORT\_ERROR

    This function throws an exception.

    The function prototype of  **PKG\_UTIL.EXCEPTION\_REPORT\_ERROR**  is as follows:

    ```
    PKG_UTIL.EXCEPTION_REPORT_ERROR(
    code integer,
    log text,
    flag boolean DEFAULT false
    )
    RETURN INTEGER
    ```

    **Table  36**  PKG\_UTIL.EXCEPTION\_REPORT\_ERROR interface parameters

    <a name="table1391731317366"></a>
    <table><thead align="left"><tr id="row199189136364"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1918713143617"><a name="p1918713143617"></a><a name="p1918713143617"></a>Parameter</p>
    </th>
    <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p29181613133615"><a name="p29181613133615"></a><a name="p29181613133615"></a>Description</p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row12918513163617"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1591811316362"><a name="p1591811316362"></a><a name="p1591811316362"></a>code</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p091881312362"><a name="p091881312362"></a><a name="p091881312362"></a>Error code displayed when an exception occurs.</p>
    </td>
    </tr>
    <tr id="row10662152612275"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p19662102618276"><a name="p19662102618276"></a><a name="p19662102618276"></a>log</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p2662102611271"><a name="p2662102611271"></a><a name="p2662102611271"></a>Log information displayed when an exception occurs.</p>
    </td>
    </tr>
    <tr id="row8726182110274"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p187271021192714"><a name="p187271021192714"></a><a name="p187271021192714"></a>flag</p>
    </td>
    <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1872762114275"><a name="p1872762114275"></a><a name="p1872762114275"></a>Reserved. The default value is <strong id="b7588829144316"><a name="b7588829144316"></a><a name="b7588829144316"></a>false</strong>.</p>
    </td>
    </tr>
    </tbody>
    </table>

    -   PKG\_UTIL.app\_read\_client\_info

        Reads the client information.

        The prototype of the  **PKG\_UTIL.app\_read\_client\_info**  function is as follows:

        ```
        PKG_UTIL.app_read_client_info(
        OUT buffer text
        )
        ```

        **Table  37**  PKG\_UTIL.app\_read\_client\_info parameters

        <a name="table1838365020255"></a>
        <table><thead align="left"><tr id="row1383195082515"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p93838504252"><a name="p93838504252"></a><a name="p93838504252"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p1638419507256"><a name="p1638419507256"></a><a name="p1638419507256"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row1384185022511"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p16384115092515"><a name="p16384115092515"></a><a name="p16384115092515"></a>buffer</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p188415524416"><a name="p188415524416"></a><a name="p188415524416"></a>Client information returned.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.app\_set\_client\_info

        Sets the client information.

        The prototype of the  **PKG\_UTIL.app\_set\_client\_info**  function is as follows:

        ```
        PKG_UTIL.app_set_client_info(
        str text
        )
        RETURN INTEGER
        ```

        **Table  38**  PKG\_UTIL.app\_set\_client\_info parameters

        <a name="table3862141518468"></a>
        <table><thead align="left"><tr id="row148621215114611"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p128624151466"><a name="p128624151466"></a><a name="p128624151466"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p10862141512468"><a name="p10862141512468"></a><a name="p10862141512468"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row20862151584611"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p9862915174620"><a name="p9862915174620"></a><a name="p9862915174620"></a>str</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p15862151524615"><a name="p15862151524615"></a><a name="p15862151524615"></a>Client information to be set.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.lob\_converttoblob

        Converts a CLOB to a BLOB.  **amout**  indicates the conversion length.

        The prototype of the  **PKG\_UTIL.lob\_converttoblob**  function is as follows:

        ```
        PKG_UTIL.lob_converttoblob(
        dest_lob blob, 
        src_clob clob, 
        amount integer, 
        dest_offset integer, 
        src_offset integer
        )
        ```

        **Table  39**  PKG\_UTIL.lob\_converttoblob parameters

        <a name="table8302151884916"></a>
        <table><thead align="left"><tr id="row1130219188491"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p130241844911"><a name="p130241844911"></a><a name="p130241844911"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p7303191818490"><a name="p7303191818490"></a><a name="p7303191818490"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row16303161834919"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p630316189495"><a name="p630316189495"></a><a name="p630316189495"></a>dest_lob</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p23471025115315"><a name="p23471025115315"></a><a name="p23471025115315"></a>Target LOB.</p>
        </td>
        </tr>
        <tr id="row147890513508"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p9789135116503"><a name="p9789135116503"></a><a name="p9789135116503"></a>src_clob</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p278912516503"><a name="p278912516503"></a><a name="p278912516503"></a>CLOB to be converted.</p>
        </td>
        </tr>
        <tr id="row6369548125019"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p15369148175010"><a name="p15369148175010"></a><a name="p15369148175010"></a>amount</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p736994855017"><a name="p736994855017"></a><a name="p736994855017"></a>Conversion length.</p>
        </td>
        </tr>
        <tr id="row563585515017"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1763620554501"><a name="p1763620554501"></a><a name="p1763620554501"></a>dest_offset</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p363625515016"><a name="p363625515016"></a><a name="p363625515016"></a>Start position of the target LOB.</p>
        </td>
        </tr>
        <tr id="row548295910503"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p184822597503"><a name="p184822597503"></a><a name="p184822597503"></a>src_offset</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p5482659165013"><a name="p5482659165013"></a><a name="p5482659165013"></a>Start position of the source CLOB.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.lob\_converttoclob

        Converts a BLOB to a CLOB.  **amout**  indicates the conversion length.

        The prototype of the  **PKG\_UTIL.lob\_converttoclob**  function is as follows:

        ```
        PKG_UTIL.lob_converttoclob(
        dest_lob clob, 
        src_blob blob, 
        amount integer, 
        dest_offset integer, 
        src_offset integer
        )
        ```

        **Table  40**  PKG\_UTIL.lob\_converttoclob parameters

        <a name="table1919438165712"></a>
        <table><thead align="left"><tr id="row1519417875719"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p819419825720"><a name="p819419825720"></a><a name="p819419825720"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p1319416814574"><a name="p1319416814574"></a><a name="p1319416814574"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row0195884571"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1619510813574"><a name="p1619510813574"></a><a name="p1619510813574"></a>dest_lob</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p719538165718"><a name="p719538165718"></a><a name="p719538165718"></a>Target LOB.</p>
        </td>
        </tr>
        <tr id="row1919518195714"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p19195208155717"><a name="p19195208155717"></a><a name="p19195208155717"></a>src_blob</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p15195168135714"><a name="p15195168135714"></a><a name="p15195168135714"></a>BLOB to be converted.</p>
        </td>
        </tr>
        <tr id="row919511811579"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p6195086579"><a name="p6195086579"></a><a name="p6195086579"></a>amount</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p101953835710"><a name="p101953835710"></a><a name="p101953835710"></a>Conversion length.</p>
        </td>
        </tr>
        <tr id="row4195386573"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1619528115713"><a name="p1619528115713"></a><a name="p1619528115713"></a>dest_offset</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p15196168185710"><a name="p15196168185710"></a><a name="p15196168185710"></a>Start position of the target LOB.</p>
        </td>
        </tr>
        <tr id="row419612895716"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p719614845717"><a name="p719614845717"></a><a name="p719614845717"></a>src_offset</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p61967885711"><a name="p61967885711"></a><a name="p61967885711"></a>Start position of the source CLOB.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.lob\_texttoraw

        Converts from the text type to the raw type.

        The prototype of the  **PKG\_UTIL.lob\_texttoraw**  function is as follows:

        ```
        PKG_UTIL.lob_texttoraw(
        src_lob clob
        )
        RETURN raw
        ```

        **Table  41**  PKG\_UTIL.lob\_texttoraw parameters

        <a name="table75108314817"></a>
        <table><thead align="left"><tr id="row19510031586"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p13510131188"><a name="p13510131188"></a><a name="p13510131188"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p7510731785"><a name="p7510731785"></a><a name="p7510731785"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row65119311813"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1351193119811"><a name="p1351193119811"></a><a name="p1351193119811"></a>src_lob</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p65111131286"><a name="p65111131286"></a><a name="p65111131286"></a>LOB to be converted.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.match\_edit\_distance\_similarity

        Calculates the difference between two character strings.

        The prototype of the  **PKG\_UTIL.match\_edit\_distance\_similarity**  function is as follows:

        ```
        PKG_UTIL.match_edit_distance_similarity(
        str1 text, 
        str2 text
        )
        RETURN INTEGER
        ```

        **Table  42**  PKG\_UTIL.match\_edit\_distance\_similarity parameters

        <a name="table1395501716116"></a>
        <table><thead align="left"><tr id="row295661761119"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p0956417111120"><a name="p0956417111120"></a><a name="p0956417111120"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p14956181751113"><a name="p14956181751113"></a><a name="p14956181751113"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row595651731118"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p13956141714114"><a name="p13956141714114"></a><a name="p13956141714114"></a>str1</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p3956161771110"><a name="p3956161771110"></a><a name="p3956161771110"></a>First character string.</p>
        </td>
        </tr>
        <tr id="row4436124418121"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p134361444111218"><a name="p134361444111218"></a><a name="p134361444111218"></a>str2</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1543610443126"><a name="p1543610443126"></a><a name="p1543610443126"></a>Second character string.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.raw\_cast\_to\_varchar2

        Converts from the raw type to the varchar2 type.

        The prototype of the  **PKG\_UTIL.raw\_cast\_to\_varchar2**  function is as follows:

        ```
        PKG_UTIL.raw_cast_to_varchar2(
        str1 text, 
        str2 text
        )
        RETURN INTEGER
        ```

        **Table  43**  PKG\_UTIL.raw\_cast\_to\_varchar2 parameters

        <a name="table495474216130"></a>
        <table><thead align="left"><tr id="row1695415423138"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p19954164241310"><a name="p19954164241310"></a><a name="p19954164241310"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p13954144201316"><a name="p13954144201316"></a><a name="p13954144201316"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row2095474261318"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p59551042101316"><a name="p59551042101316"></a><a name="p59551042101316"></a>str1</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1895574251312"><a name="p1895574251312"></a><a name="p1895574251312"></a>First character string.</p>
        </td>
        </tr>
        <tr id="row179553425130"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p129551142111311"><a name="p129551142111311"></a><a name="p129551142111311"></a>str2</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p179551542111311"><a name="p179551542111311"></a><a name="p179551542111311"></a>Second character string.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.session\_clear\_context

        Clears the session context.

        The prototype of the  **PKG\_UTIL.session\_clear\_context**  function is as follows:

        ```
        PKG_UTIL.session_clear_context(
        namespace text, 
        client_identifier text, 
        attribute text
        )
        RETURN INTEGER
        ```

        **Table  44**  PKG\_UTIL.session\_clear\_context parameters

        <a name="table697442914241"></a>
        <table><thead align="left"><tr id="row1297452912415"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p497492972416"><a name="p497492972416"></a><a name="p497492972416"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p2974129172413"><a name="p2974129172413"></a><a name="p2974129172413"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row0974182911244"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p15974172932412"><a name="p15974172932412"></a><a name="p15974172932412"></a>namespace</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p3991849263"><a name="p3991849263"></a><a name="p3991849263"></a>Namespace of an attribute.</p>
        </td>
        </tr>
        <tr id="row1897512294241"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p897512292247"><a name="p897512292247"></a><a name="p897512292247"></a>client_identifier</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p197520295248"><a name="p197520295248"></a><a name="p197520295248"></a>Usually the value of this parameter is the same as that of <strong id="b467572051919"><a name="b467572051919"></a><a name="b467572051919"></a>namespace</strong>. If this parameter is set to <strong id="b89811829141918"><a name="b89811829141918"></a><a name="b89811829141918"></a>null</strong>, all namespaces are modified by default.</p>
        </td>
        </tr>
        <tr id="row1986715272250"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p38671279257"><a name="p38671279257"></a><a name="p38671279257"></a>attribute</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p158673277259"><a name="p158673277259"></a><a name="p158673277259"></a>Attribute to be cleared.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.session\_search\_context

        Searches for an attribute value.

        The prototype of the  **PKG\_UTIL.session\_clear\_context**  function is as follows:

        ```
        PKG_UTIL.session_clear_context(
        namespace text, 
        attribute text
        )
        RETURN INTEGER
        ```

        **Table  45**  PKG\_UTIL.session\_clear\_context parameters

        <a name="table88623285274"></a>
        <table><thead align="left"><tr id="row20862172819279"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p1486272832718"><a name="p1486272832718"></a><a name="p1486272832718"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p13862328192712"><a name="p13862328192712"></a><a name="p13862328192712"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row18634289273"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p15863528182714"><a name="p15863528182714"></a><a name="p15863528182714"></a>namespace</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p138631428142716"><a name="p138631428142716"></a><a name="p138631428142716"></a>Namespace of an attribute.</p>
        </td>
        </tr>
        <tr id="row5863162811271"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1186392822713"><a name="p1186392822713"></a><a name="p1186392822713"></a>attribute</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p1386392811277"><a name="p1386392811277"></a><a name="p1386392811277"></a>Attribute to be cleared.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.session\_set\_context

        Sets the attribute value.

        The prototype of the  **PKG\_UTIL.session\_set\_context**  function is as follows:

        ```
        PKG_UTIL.session_set_context(
        namespace text, 
        attribute text,
        value text
        )
        RETURN INTEGER
        ```

        **Table  46**  PKG\_UTIL.session\_set\_context parameters

        <a name="table611731214321"></a>
        <table><thead align="left"><tr id="row6117161243216"><th class="cellrowborder" valign="top" width="17.86%" id="mcps1.2.3.1.1"><p id="p81178121320"><a name="p81178121320"></a><a name="p81178121320"></a>Parameter</p>
        </th>
        <th class="cellrowborder" valign="top" width="82.14%" id="mcps1.2.3.1.2"><p id="p41175124328"><a name="p41175124328"></a><a name="p41175124328"></a>Description</p>
        </th>
        </tr>
        </thead>
        <tbody><tr id="row91172126321"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p7117212103213"><a name="p7117212103213"></a><a name="p7117212103213"></a>namespace</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p10117171217322"><a name="p10117171217322"></a><a name="p10117171217322"></a>Namespace of an attribute.</p>
        </td>
        </tr>
        <tr id="row411812127328"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1911831243217"><a name="p1911831243217"></a><a name="p1911831243217"></a>attribute</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p16277145213312"><a name="p16277145213312"></a><a name="p16277145213312"></a>Attribute to be set.</p>
        </td>
        </tr>
        <tr id="row1320681718331"><td class="cellrowborder" valign="top" width="17.86%" headers="mcps1.2.3.1.1 "><p id="p1020621710335"><a name="p1020621710335"></a><a name="p1020621710335"></a>value</p>
        </td>
        <td class="cellrowborder" valign="top" width="82.14%" headers="mcps1.2.3.1.2 "><p id="p398316311337"><a name="p398316311337"></a><a name="p398316311337"></a>Attribute value.</p>
        </td>
        </tr>
        </tbody>
        </table>

    -   PKG\_UTIL.utility\_get\_time

        Prints the Unix timestamp.

        The prototype of the  **PKG\_UTIL.utility\_get\_time**  function is as follows:

        ```
        PKG_UTIL.utility_get_time()
        RETURN text
        ```

    -   PKG\_UTIL.utility\_format\_error\_backtrace

        Displays the error stack of a stored procedure.

        The prototype of the  **PKG\_UTIL.utility\_format\_error\_backtrace**  function is as follows:

        ```
        PKG_UTIL.utility_format_error_backtrace()
        RETURN text
        ```

    -   PKG\_UTIL.utility\_format\_error\_stack

        Displays the error information of a stored procedure.

        The prototype of the  **PKG\_UTIL.utility\_format\_error\_stack**  function is as follows:

        ```
        PKG_UTIL.utility_format_error_stack()
        RETURN text
        ```

    -   PKG\_UTIL.utility\_format\_call\_stack

        Displays the call stack of a stored procedure.

        The prototype of the  **PKG\_UTIL.utility\_format\_call\_stack**  function is as follows:

        ```
        PKG_UTIL.utility_format_call_stack()
        RETURN text
        ```



