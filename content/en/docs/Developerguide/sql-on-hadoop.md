# SQL on Hadoop<a name="EN-US_TOPIC_0311524271"></a>

In openGauss, you can use FDW to access external data sources. You can also create read-only foreign tables containing structured data in HDFS, which inherit all the existing SQL query syntax and capabilities, to query HDFS data.

-   **[Interconnection Configuration](interconnection-configuration.md)**  

-   **[Accessing HDFS Data Using a Foreign Table](accessing-hdfs-data-using-a-foreign-table.md)**  

-   **[Supported Data Types](supported-data-types.md)**  

-   **[Statistics Collection](statistics-collection.md)**  

-   **[Foreign Table Usage](foreign-table-usage.md)**  

-   **[Querying HDFS Data Using the Acceleration Database Instance](querying-hdfs-data-using-the-acceleration-database-instance.md)**  


