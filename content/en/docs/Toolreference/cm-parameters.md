# CM Parameters<a name="EN-US_TOPIC_0000001201167154"></a>

You can view cm\_agent parameters in the  **cm\_agent.conf**  file in the cm\_agent data directory and cm\_server parameters in the  **cm\_server.conf**  file in the cm\_server data directory.

-   **[Parameters Related to cm\_agent](parameters-related-to-cm_agent.md)**  

-   **[Parameters Related to cm\_server](parameters-related-to-cm_server.md)**  


