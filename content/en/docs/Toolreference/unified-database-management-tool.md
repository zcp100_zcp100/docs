# Unified Database Management Tool<a name="EN-US_TOPIC_0000001246286999"></a>

The cluster manager \(CM\) is a database management module. It supports customized resource monitoring and provides capabilities such as monitoring of the primary/standby database status, network communication faults, file system faults, and automatic primary/standby switchover upon faults. It also provides various database management capabilities, such as starting and stopping nodes and instances, querying database instance status, performing primary/standby switchover, and managing logs.

Note that in a scenario where there are one primary node and one standby node, the CM supports only basic capabilities, such as installation, startup, stop, and detection.

-   **[Features](features.md)**  

-   **[Introduction to the cm\_ctl Tool](introduction-to-the-cm_ctl-tool.md)**  

-   **[Security Design](security-design.md)**  

-   **[CM Parameters](cm-parameters.md)**  


