# Simplified Installation Process<a name="EN-US_TOPIC_0000001091808516"></a>

The simplified installation process is intended for users in universities and testing. Compared with the enterprise-edition installation process, this process is simpler and quicker.

-   **[Preparing for Installation](preparing-for-installation1.md)**  

-   **[Installation on a Single Node](installation-on-a-single-node.md)**  

-   **[Installation on Primary/Standby Nodes](installation-on-primary-standby-nodes.md)**  

---

-   **[Quick experience on Docker(Available in Windows)](quick-experience-on-Docker.md)**  

