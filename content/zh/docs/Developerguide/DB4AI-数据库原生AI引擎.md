# DB4AI：数据库原生AI引擎<a name="ZH-CN_TOPIC_0000001102506722"></a>

DB4AI是指利用数据库的能力驱动AI任务，实现数据存储、技术栈的同构。通过在数据库内集成AI算法，令openGauss具备数据库原生AI计算引擎、模型管理、AI算子、AI原生执行计划的能力，为用户提供普惠AI技术。不同于传统的AI建模流程，DB4AI“一站式”建模可以解决数据在各平台的反复流转问题，同时简化开发流程，并可通过数据库规划出最优执行路径，让开发者更专注于具体业务和模型的调优上，具备同类产品不具备的易用性与性能优势。

-   **[DB4AI-Snapshots数据版本管理](DB4AI-Snapshots数据版本管理.md)**  

-   **[DB4AI-Query：模型训练和推断](DB4AI-Query-模型训练和推断.md)**  

-   **[plpython-fenced模式](plpython-fenced模式.md)**  


