# Query<a name="EN-US_TOPIC_0289900016"></a>

-   **[STATEMENT](statement.md)**  

-   **[SUMMARY\_STATEMENT](summary_statement.md)**  

-   **[STATEMENT\_COUNT](statement_count.md)**  

-   **[GLOBAL\_STATEMENT\_COUNT](global_statement_count.md)**  

-   **[SUMMARY\_STATEMENT\_COUNT](summary_statement_count.md)**  

-   **[GLOBAL\_STATEMENT\_COMPLEX\_HISTORY](global_statement_complex_history.md)**  

-   **[GLOBAL\_STATEMENT\_COMPLEX\_HISTORY\_TABLE](global_statement_complex_history_table.md)**  

-   **[GLOBAL\_STATEMENT\_COMPLEX\_RUNTIME](global_statement_complex_runtime.md)**  

-   **[STATEMENT\_RESPONSETIME\_PERCENTILE](statement_responsetime_percentile.md)**  

-   **[STATEMENT\_USER\_COMPLEX\_HISTORY](statement_user_complex_history.md)**  

-   **[STATEMENT\_COMPLEX\_RUNTIME](statement_complex_runtime.md)**  

-   **[STATEMENT\_COMPLEX\_HISTORY\_TABLE](statement_complex_history_table.md)**  

-   **[STATEMENT\_COMPLEX\_HISTORY](statement_complex_history.md)**  

-   **[STATEMENT\_WLMSTAT\_COMPLEX\_RUNTIME](statement_wlmstat_complex_runtime.md)**  

-   **[STATEMENT\_HISTORY](statement_history.md)**  

-   **[GS\_SLOW\_QUERY\_INFO \(废弃\)](gs_slow_query_info_废弃.md)**  

-   **[GS\_SLOW\_QUERY\_HISTORY \(废弃\)](gs_slow_query_history_废弃.md)**  

-   **[GLOBAL\_SLOW\_QUERY\_HISTORY \(废弃\)](global_slow_query_history_废弃.md)**  

-   **[GLOBAL\_SLOW\_QUERY\_INFO \(废弃\)](global_slow_query_info_废弃.md)**  


