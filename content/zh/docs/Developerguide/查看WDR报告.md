# 查看WDR报告<a name="ZH-CN_TOPIC_0000001215195224"></a>

-   **[Database Stat](Database-Stat.md)**  

-   **[Load Profile](Load-Profile.md)**  

-   **[Instance Efficiency Percentages](Instance-Efficiency-Percentages.md)**  

-   **[Top 10 Events by Total Wait Time](Top-10-Events-by-Total-Wait-Time.md)**  

-   **[Wait Classes by Total Wait Time](Wait-Classes-by-Total-Wait-Time.md)**  

-   **[Host CPU](Host-CPU.md)**  

-   **[IO Profile](IO-Profile.md)**  

-   **[Memory Statistics](Memory-Statistics.md)**  

-   **[Time Model](Time-Model.md)**  

-   **[SQL Statistics](SQL-Statistics.md)**  

-   **[Wait Events](Wait-Events.md)**  

-   **[Cache IO Stats](Cache-IO-Stats.md)**  

-   **[Utility status](Utility-status.md)**  

-   **[Object stats](Object-stats.md)**  

-   **[Configuration settings](Configuration-settings.md)**  

-   **[SQL Detail](SQL-Detail.md)**  


