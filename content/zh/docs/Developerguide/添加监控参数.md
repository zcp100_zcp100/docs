# 添加监控参数<a name="ZH-CN_TOPIC_0303986185"></a>

工具只针对os\_exporter中的指标进行趋势预测与阈值异常检测，支持用户添加新的监控参数，步骤如下：

1.  在task/os\_exporter.py的OS\_exporter中编写获取指标的功能函数，并将该函数加入到output的result列表中，例如：

    ```
    @staticmethod
    def new_metric():
        return metric_value
        
    def output(self):
        result = [self.cpu_usage(), self.io_wait(), self.io_read(),
                  self.io_write(), self.memory_usage(), self.disk_space(), self.new_metric()]
        return result
    
    ```

2.  在table.json的os\_exporter中，将new\_metric字段加入到“create table“中，并在“insert”中加上字段类型信息，例如：

    ```
    "os_exporter": {
      "create_table": "create table  os_exporter(timestamp bigint, cpu_usage text, io_wait text, io_read text, io_write text, memory_usage text, disk_space text, new_metric text);",
      "insert": "insert into os_exporter values(%d, \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\");",
    ```

3.  在task/metric\_task.conf中添加指标的上限值或者下限值，例如：

    ```
    [os_exporter]
    new_metric_minimum = 0
    new_metric_maximum = 10
    ```


